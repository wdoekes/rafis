<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAFIS license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="banshee/main.xslt" />
<xsl:include href="includes/standards.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="links">
<xsl:apply-templates select="standards" />

<ul class="tabs">
<li class="selected" id="threat" onClick="show_threats()">Threats</li>
<li id="control" onClick="show_controls()">Controls</li>
</ul>

<div class="threats">
<p>All threats and their linked <xsl:value-of select="standards/standard[@selected='yes']" /> controls.</p>
<xsl:for-each select="threats/threat">
<xsl:if test="category">
<div class="item">
<h3><xsl:value-of select="category" /></h3>
</div>
</xsl:if>
<div class="item">
<div class="head" onClick="javascript:$('.controls_{@id}').slideToggle('normal')">
	<xsl:value-of select="number" />. <xsl:value-of select="threat" />
</div>
<div class="links controls_{@id}">
<p><xsl:value-of select="description" /><br />
Availability: <xsl:value-of select="availability" />, Integrity: <xsl:value-of select="integrity" />, Confidentiality: <xsl:value-of select="confidentiality" /></p>
<ul>
<xsl:for-each select="control">
<li><xsl:value-of select="." /></li>
</xsl:for-each>
</ul>
</div>
</div>
</xsl:for-each>
</div>

<div class="controls">
<p>All <xsl:value-of select="standards/standard[@selected='yes']" /> controls and their linked threats.</p>
<xsl:for-each select="controls/control">
<xsl:if test="category">
<div class="item">
<h3><xsl:value-of select="category" /></h3>
</div>
</xsl:if>
<div class="item">
<div class="head" onClick="javascript:$('.threats_{@id}').slideToggle('normal')">
	<xsl:value-of select="number" />&#160;<xsl:value-of select="control" />
</div>
<div class="links threats_{@id}">
<p>Reduces the <xsl:value-of select="reduce" /> of an incident.</p>
<ul>
<xsl:for-each select="threat">
<li><xsl:value-of select="." /></li>
</xsl:for-each>
</ul>
</div>
</div>
</xsl:for-each>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>Links between threats and controls</h1>
<xsl:apply-templates select="links" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
