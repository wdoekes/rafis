<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAFIS license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<div class="row">
<div class="col-sm-6">

<div class="panel panel-default">
<div class="panel-heading">Export data</div>
<div class="panel-body">
<form action="/{/output/page}" method="post">
<input type="submit" name="submit_button" value="Export" class="btn btn-default" />
</form>
</div>
</div>

</div>
<div class="col-sm-6">

<div class="panel panel-default">
<div class="panel-heading">Import data</div>
<div class="panel-body">
<form action="/{/output/page}" method="post" enctype="multipart/form-data">
<div class="input-group">
<span class="input-group-btn"><label class="btn btn-default">
<input type="file" name="file" style="display:none" class="form-control" onChange="$('#upload-file-info').val(this.files[0].name)" />Browse</label></span>
<input type="text" id="upload-file-info" readonly="readonly" class="form-control" />
<span class="input-group-btn"><input type="submit" name="submit_button" value="Import" class="btn btn-default" onClick="javascript:return confirm('This action replaces all current data (BIA, actors en cases) with the import. Continue?')" /></span>
</div>
</form>
</div>
</div>

</div>
</div>

<div class="btn-group">
<a href="/dashboard" class="btn btn-default">Back</a>
</div>

<div id="help">
<p>You are responsible for securing your risk assessments. Therefore, always make an export of your data after performing a risk analysis. An export contains the BIAs, the actors and the risk analyses. User data is not exported.</p>
<p>When importing a RAFIS export, the existing BIA, actors and risk analysis cases are first removed.</p>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/data.png" class="title_icon" />
<h1>Data management</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
