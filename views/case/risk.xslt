<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Assess template
//
//-->
<xsl:template match="assess">
<table class="matrix">
<tr><td></td><td></td><td colspan="{count(matrix/row)-1}">Impact</td></tr>
<xsl:for-each select="matrix/row">
<tr>
	<xsl:if test="position()=1"><td></td></xsl:if>
	<xsl:if test="position()=2"><td rowspan="{count(../row)-1}" class="chance">Chance</td></xsl:if>
	<xsl:for-each select="cell">
		<td><xsl:if test="@class"><xsl:attribute name="class"><xsl:value-of select="@class" /></xsl:attribute></xsl:if><xsl:value-of select="." /></td>
	</xsl:for-each>
</tr>
</xsl:for-each>
</table>

<form action="/{/output/page}" method="post">
<div class="btn-group">
<a href="/{/output/page}" class="btn btn-default">Refresh matrix</a>
<input type="submit" name="submit_button" value="Empty matrix" class="btn btn-default" />
</div>
<div class="btn-group right">
<input type="submit" name="submit_button" value="New access code" class="btn btn-default" onClick="javascript:return confirm('Are you sure?')" />
</div>
</form>

<div id="help">
<p>Have participants of the risk analysis session visit the following URL:</p>
<span>https://<xsl:value-of select="/output/website_url" />/risk</span>
<p>The required access code is:</p>
<span><xsl:value-of select="access_code" /></span>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>Risk assessment</h1>
<xsl:apply-templates select="assess" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
