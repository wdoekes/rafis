<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAFIS license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover">
<thead>
<tr>
<th>Scenario</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="scenarios/scenario">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{../../../case/@id}/{@id}'">
<td><xsl:value-of select="title" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left">
<a href="/{/output/page}/{../case/@id}/new" class="btn btn-default">New scenario</a>
</div>
<div class="btn-group right">
<a href="/case/report/{../case/@id}" class="btn btn-default">Continue to report</a>
</div>

<div id="help">
<p>Consider how multiple threats together can lead to an organization-disrupting incident, where small and/or simple threats serve as a stepping stone to a threat with a large impact.</p>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />

<!-- Actors -->
<h2>Actors<span class="show_table" onClick="javascript:$('table.actors').toggle()">+</span></h2>
<table class="table table-condensed table-striped table-xs actors">
<thead class="table-xs">
<tr><th>Actor</th><th>Willingness / chance</th><th>Knowledge</th><th>Resources</th><th>Threats</th></tr>
</thead>
<tbody>
<xsl:for-each select="actors/actor">
<tr>
<td><span class="table-xs">Actor</span><xsl:value-of select="name" /></td>
<td><span class="table-xs">Willingness / chance</span><xsl:value-of select="chance" /></td>
<td><span class="table-xs">Knowledge</span><xsl:value-of select="knowledge" /></td>
<td><span class="table-xs">Resources</span><xsl:value-of select="resources" /></td>
<td><span class="table-xs">Threat</span><xsl:value-of select="threat" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<!-- Threats -->
<h2>Threats<span class="show_table" onClick="javascript:$('table.threats').toggle()">+</span></h2>
<table class="table table-condensed table-striped table-xs threats">
<thead class="table-xs">
<tr>
<th>Threat</th>
<th>Chance</th>
<th>Impact</th>
<th>Urgency</th>
<th>Approach</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="threats/threat">
<tr>
<td><span class="table-xs">Threat</span><xsl:value-of select="threat" /></td>
<td><span class="table-xs">Chance</span><xsl:value-of select="chance" /></td>
<td><span class="table-xs">Impact</span><xsl:value-of select="impact" /></td>
<td><span class="table-xs">Urgency</span><span class="urgency{risk_value}"><xsl:value-of select="risk_label" /></span></td>
<td><span class="table-xs">Approach</span><xsl:value-of select="handle" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<!-- BIA -->
<h2>BIA<span class="show_table" onClick="javascript:$('table.bia').toggle()">+</span></h2>
<table class="table table-condensed table-striped table-xs bia">
<thead class="table-xs">
<tr><th>System</th><th>Impact of incident</th></tr>
</thead>
<tbody>
<xsl:for-each select="bia/item">
<tr>
<td><span class="table-xs">System</span><xsl:value-of select="item" /></td>
<td><span class="table-xs">Impact of incident</span><xsl:value-of disable-output-escaping="yes" select="impact" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<form action="/{/output/page}/{../case/@id}" method="post">
<xsl:if test="scenario/@id">
<input type="hidden" name="id" value="{scenario/@id}" />
</xsl:if>

<label for="title">Title:</label>
<input type="text" id="title" name="title" value="{scenario/title}" class="form-control" />
<label for="scenario">Scenario:</label>
<textarea id="scenario" name="scenario" class="form-control"><xsl:value-of select="scenario/scenario" /></textarea>
<label for="consequences">Possible consequences:</label>
<textarea id="consequences" name="consequences" class="form-control"><xsl:value-of select="scenario/consequences" /></textarea>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save scenario" class="btn btn-default" />
<a href="/{/output/page}/{../case/@id}" class="btn btn-default">Cancel</a>
<xsl:if test="scenario/@id">
<input type="submit" name="submit_button" value="Delete scenario" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="breadcrumbs" />
<h1>Scenarios</h1>
<div class="case"><xsl:value-of select="case" /></div>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
