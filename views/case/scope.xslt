<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAFIS license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<form action="/{/output/page}/{../case/@id}" method="post">
<table class="table table-condensed table-striped table-xs scope">
<thead class="table-xs">
<tr>
<th>Information system</th>
<th>Value</th>
<th>Owner</th>
<th>PII</th>
<th>Location</th>
<th class="visible last"></th>
<th class="hidden last">Scope</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="item[scope='yes']">
<tr>
<td><span class="table-xs">Information system</span><xsl:value-of select="item" /></td>
<td><span class="table-xs">Value</span><xsl:value-of select="value" /></td>
<td><span class="table-xs">Owner</span><xsl:value-of select="owner" /></td>
<td><span class="table-xs">PII</span><xsl:value-of select="personal_data" /></td>
<td><span class="table-xs">Location</span><xsl:value-of select="location" /></td>
<td class="visible"></td>
<td class="hidden"><input type="checkbox" name="scope[]" value="{@id}" checked="checked" /></td>
</tr>
</xsl:for-each>
<xsl:for-each select="item[scope='no']">
<tr class="hidden">
<td><xsl:value-of select="item" /></td>
<td><xsl:value-of select="value" /></td>
<td><xsl:value-of select="owner" /></td>
<td><xsl:value-of select="personal_data" /></td>
<td><xsl:value-of select="location" /></td>
<td class="visible"></td>
<td><input type="checkbox" name="scope[]" value="{@id}" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left visible">
<input type="button" value="Edit" class="btn btn-default" onClick="javascript:enable_form()" />
</div>
<div class="btn-group left hidden">
<input type="submit" name="submit_button" value="Save" class="btn btn-default" />
<input type="button" value="Cancel" class="btn btn-default" onClick="javascript:disable_form()" />
</div>
</form>

<xsl:if test="count(item[scope='yes'])>0">
<div class="btn-group right visible">
<a href="/case/interests/{../case/@id}" class="btn btn-default">Continue to interests</a>
</div>
</xsl:if>

<div id="help">
<p>Select systems that lie within the scope of this risk analysis. The systems in the list are the systems that have been added in the BIA section.</p>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="breadcrumbs" />
<h1>Scope</h1>
<div class="case"><xsl:value-of select="case" /></div>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
