<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAFIS license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<xsl:if test="@archive='yes'">
<h3>Archive</h3>
</xsl:if>
<form action="/{/output/page}" method="post">
<table class="table table-condensed table-striped">
<thead>
<tr>
<th>Subject</th>
<th>Standard</th>
<th>Date</th>
<th></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="case">
<tr>
<td><xsl:if test="organisation!=''"><xsl:value-of select="organisation" /> :: </xsl:if><xsl:value-of select="name" /></td>
<td><xsl:value-of select="standard" /></td>
<td><xsl:value-of select="date" /></td>
<td><a href="/{start}/{@id}" id="start" class="btn btn-xs btn-primary">Start</a> <a href="/{/output/page}/{@id}" class="btn btn-xs btn-default">Edit</a></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left">
<xsl:if test="@archive='no'">
<a href="/{/output/page}/new" class="btn btn-default">New case</a>
<xsl:if test="@archived>0">
<a href="/{/output/page}/archive" class="btn btn-default">Show archive</a>
</xsl:if>
<a href="/dashboard" class="btn btn-default">Back</a>
</xsl:if>
<xsl:if test="@archive='yes'">
<a href="/{/output/page}" class="btn btn-default">Leave archive</a>
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="case/@id">
<input type="hidden" name="id" value="{case/@id}" />
</xsl:if>

<div class="row">
<div class="col-md-6">
<label for="name">Organization(al unit):</label>
<input type="text" id="organisation" name="organisation" value="{case/organisation}" class="form-control" />
<label for="name">Case name (hint: scope in one word):</label>
<input type="text" id="name" name="name" value="{case/name}" class="form-control" />
<label for="standard_id">Standard:</label>
<select id="standard_id" name="standard_id" class="form-control">
<xsl:for-each select="standards/standard">
<option value="{@id}"><xsl:if test="@id=../../case/standard_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
<label for="date">Date:</label>
<input type="text" id="date" name="date" value="{case/date}" class="form-control datepicker" />
<label for="scope">Scope. The risk analysis was limited to ...</label>
<textarea id="scope" name="scope" class="form-control"><xsl:value-of select="case/scope" /></textarea>
<label for="logo">URL of logo to be used in report:</label>
<input type="text" id="logo" name="logo" value="{case/logo}" class="form-control" />
<xsl:if test="case/@id">
<label for="archived">Achived:</label>
<input type="checkbox" id="archived" name="archived"><xsl:if test="case/archived='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input>
</xsl:if>
</div>

<div class="col-md-6">
<h3>Interpretation of the impact</h3>
<p>Specify the expected amount of financial damage and/or the damage to the organization's image here.</p>
<xsl:for-each select="impact/value">
<label for="impact_{position()}"><xsl:value-of select="@label" />:</label>
<input type="text" id="impact_{position()}" name="impact[]" value="{.}" class="form-control" />
</xsl:for-each>
</div>
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save case" class="btn btn-default" />
<xsl:if test="case/archived='no'">
<a href="/{/output/page}" class="btn btn-default">Cancel</a>
</xsl:if>
<xsl:if test="case/archived='yes'">
<a href="/{/output/page}/archive" class="btn btn-default">Cancel</a>
</xsl:if>
<xsl:if test="case/@id">
<input type="submit" name="submit_button" value="Delete case" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/case.png" class="title_icon" />
<h1>Cases</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
