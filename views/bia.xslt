<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAFIS license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover table-xs bia">
<thead class="table-xs">
<tr>
<th>Information system</th>
<th>Confidentiality</th>
<th>Integrity</th>
<th>Availability</th>
<th>Value</th>
<th>Owner</th>
<th>PII</th>
<th>Location</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="item">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{@id}'">
<td><span class="table-xs">Information system</span><xsl:value-of select="item" /></td>
<td><span class="table-xs">Confidentiality</span><xsl:value-of select="confidentiality" /></td>
<td><span class="table-xs">Integrity</span><xsl:value-of select="integrity" /></td>
<td><span class="table-xs">Availablity</span><xsl:value-of select="availability" /></td>
<td><span class="table-xs">Value</span><xsl:value-of select="value" /></td>
<td><span class="table-xs">Onwer</span><xsl:value-of select="owner" /></td>
<td><span class="table-xs">PII</span><xsl:value-of select="personal_data" /></td>
<td><span class="table-xs">Location</span><xsl:value-of select="location" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group">
<a href="/{/output/page}/new" class="btn btn-default">New information system</a>
<a href="/dashboard" class="btn btn-default">Back</a>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="item/@id">
<input type="hidden" name="id" value="{item/@id}" />
</xsl:if>

<label for="item">Information system:</label>
<input type="text" id="item" name="item" value="{item/item}" class="form-control" />
<label for="description">Description:</label>
<textarea id="description" name="description" class="form-control"><xsl:value-of select="item/description" /></textarea>
<label for="confidentiality">Confidentiality:</label>
<select id="confidentiality" name="confidentiality" class="form-control">
<option value="0"></option>
<xsl:for-each select="confidentiality/label">
	<option value="{@value}"><xsl:if test="@value=../../item/confidentiality"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
<label for="integrity">Integrity:</label>
<select id="integrity" name="integrity" class="form-control">
<option value="0"></option>
<xsl:for-each select="integrity/label">
	<option value="{@value}"><xsl:if test="@value=../../item/integrity"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
<label for="availability">Availability:</label>
<select id="availability" name="availability" class="form-control">
<option value="0"></option>
<xsl:for-each select="availability/label">
	<option value="{@value}"><xsl:if test="@value=../../item/availability"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
<label for="impact">Impact towards C, I and/or A in case of an incident:</label>
<textarea id="impact" name="impact" class="form-control"><xsl:value-of select="item/impact" /></textarea>
<label for="owner">Information system is assigned to an owner:</label>
<div><input type="checkbox" id="owner" name="owner"><xsl:if test="item/owner='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input></div>
<label for="owner">Information system contains personal identifiable information (PII):</label>
<div><input type="checkbox" id="personal_data" name="personal_data"><xsl:if test="item/personal_data='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input></div>
<label for="location">Location:</label>
<select id="location" name="location" class="form-control">
<xsl:for-each select="location/label">
	<option value="{@value}"><xsl:if test="@value=../../item/location"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save information system" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default">Cancel</a>
<xsl:if test="item/@id">
<input type="submit" name="submit_button" value="Delete information system" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/bia.png" class="title_icon" />
<h1>Business Impact Analysis</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
<div id="help">
<p>Use this page to specify the information systems that qualify for a risk analysis. Below is an explanation of the terms used.</p>
<h3>Availability and Integrity</h3>
<ul>
<li><b>Normal:</b> The information is not necessary for the execution of a vital process.</li>
<li><b>Important:</b> The information is necessary to a limited extent for the execution of a vital process.</li>
<li><b>Crucial:</b> The information is indispensable for the execution of a vital process.</li>
</ul>
<h3>Confidentiality</h3>
<ul>
<li><b>Public:</b> The information may be viewed by anyone.</li>
<li><b>Internal:</b> The information may only be viewed by employees and possibly a select number of business partners.</li>
<li><b>Confidential:</b> Information may only be viewed by a select number of employees. Breach of confidentiality has a serious impact on one's own organization. Confidentiality may be enforced by (privacy) legislation.</li>
<li><b>Secret:</b> The information may only be viewed by a select number of employees. Breach of confidentiality has a serious impact on one's own organization, but also on other organizations or people. Confidentiality may be enforced by law (state secrets).</li>
</ul>
<h3>Impact in case of incident</h3>
<p>If a value other than the lowest possible is entered for availability, integrity and/or confidentiality, describe here what the impact of an incident is on the organization.</p>
<h3>Value</h3>
<p>The value of the information system is automatically determined based on the entered availability, integrity and confidentiality.</p>
<h3>Owner</h3>
<p>Owner refers to a system owner. A system owner is responsible for arranging the budget, setting up a management system, making SLA agreements with the ICT department / supplier, drawing up the authorization matrix, ensuring compliance with privacy legislation (data breach reporting obligation), drawing up an emergency plan for problems with C, I and/or A, taking care of documentation and having a vision for the future.</p>
<h3>PII (Personal Identifiable Information)</h3>
<p>The system contains personal data, so the General Data Protection Regulation applies to this system. Use the result of this risk analysis during a DPIA to determine whether you comply.</p>
<h3>Location</h3>
<ul>
<li><b>Internal:</b> Application runs on its own system and is managed by its own organization.</li>
<li><b>External:</b> Application runs on an external system or within a third-party infrastructure, but is managed by its own organization.</li>
<li><b>SAAS:</b> Application runs on third party system and is managed by that third party.</li>
</ul>
</div>
</xsl:template>

</xsl:stylesheet>
