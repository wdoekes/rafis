<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  This file is part of the Banshee PHP framework
//  https://www.banshee-php.org/
//
//  Licensed under The MIT License
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />
<xsl:import href="banshee/splitform.xslt" />


<!--
//
//  Layout templates
//
//-->
<xsl:template name="splitform_header">
<h1>Register</h1>
</xsl:template>

<xsl:template name="splitform_footer">
</xsl:template>

<!--
//
//  E-mail form template
//
//-->
<xsl:template match="splitform/form_email">
<p>The registration process consists of several steps. In the first step you enter your email address. A verification code will be sent to that address, which you will need in step two for verification. In the third and final step, you enter the other necessary account information.</p>
<label for="email">E-mail address:</label>
<input type="input" id="email" name="email" value="{email}" class="form-control" />
</xsl:template>

<!--
//
//  Code form template
//
//-->
<xsl:template match="splitform/form_code">
<p>An e-mail with a verification code has been sent to your e-mail address.</p>
<label for="code">Verification code:</label>
<input type="text" name="code" value="{code}" class="form-control" />
</xsl:template>

<!--
//
//  Account form template
//
//-->
<xsl:template match="splitform/form_account">
<label for="fullname">Full name:</label>
<input type="input" id="fullname" name="fullname" value="{fullname}" class="form-control" />
<label for="username">Username:</label>
<input type="input" id="username" name="username" value="{username}" class="form-control" style="text-transform:lowercase" />
<label for="password">Password:</label>
<input type="password" id="password" name="password" class="form-control" />
<xsl:if test="../../../ask_organisation='yes'">
<label for="organisation">Organisation:</label>
<input type="text" id="organisation" name="organisation" value="{organisation}" class="form-control" />
</xsl:if>
</xsl:template>

<!--
//
//  Process template
//
//-->
<xsl:template match="submit">
<xsl:call-template name="splitform_header" />
<xsl:call-template name="progressbar" />
<p>Your account has been created. You can now log in.</p>
<xsl:call-template name="redirect"><xsl:with-param name="url" /></xsl:call-template>
</xsl:template>

</xsl:stylesheet>
