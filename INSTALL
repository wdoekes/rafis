Installation
============
This application has been built upon the Banshee PHP framework. Use the
following steps to install a Banshee based website.


Configure your webserver
------------------------
Use the directory 'public' as the webroot directory and allow PHP execution.
Also, enable URL rewriting.

For Apache, there is a .htaccess file in the 'public' directory which contains
the required URL rewriting rules. If you use another webserver, you have to
convert the rewriting rules to your webserver's configuration.


Configure PHP
-------------
Banshee requires at least PHP v7.2 and needs PHP's JSON, MySQL and XML module.
Make sure the date.timezone setting in php.ini has been set correctly.


Configure RAFIS
---------------
Decide whether you want the risk analysis data in your database to be encrypted
or not. Set ENCRYPT_DATA in settings/banshee.conf accordingly. You can't change
this setting after RAFIS has been put into use!!


Configure your database
-----------------------
Open your browser, type in the URL of your website and add /setup to it. Follow
the instructions on your screen to setup your database. After this step,
disable the setup module in settings/public_modules.conf.


Configure Cronjob
-----------------
Configure cronjob to run the followings scripts after midnight:

- database/backup_database
- database/flush_risk_assessments
- database/remove_old_logs
- send_deadline_warnings


Configure the framework/CMS
---------------------------
Go to the Settings administration page in Banshee's CMS and replace the present
e-mail addresses with your own. Before going live, set DEBUG_MODE in the file
settings/banshee.conf to 'no', change WEBSITE_DOMAIN to your own domain name
and set USE_AUTHENTICATOR to yes if you want to use an RFC 6238 based
authenticator for improved account security.
