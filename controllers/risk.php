<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class risk_controller extends Banshee\controller {
		protected $prevent_repost = false;

		private function show_options($label, $options) {
			$options = config_array($options);

			$this->view->open_tag($label);
			foreach ($options as $value => $option) {
				$this->view->add_tag("option", $option, array("value" => $value + 1));
			}
			$this->view->close_tag();
		}

		public function execute() {
			$this->view->title = "Risk assessment";

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Login") {
					if ($this->model->valid_access_code($_POST["access_code"])) {
						$_SESSION["risk_assess_access_code"] = $_POST["access_code"];
					} else {
						$this->view->add_message("Invalid access code.");
					}
				} else if ($_POST["submit_button"] == "New access code") {
					$_SESSION["risk_assess_access_code"] = null;
				}
			}

			if (isset($_SESSION["risk_assess_access_code"]) == false) {
				$this->view->add_tag("access_code");
				return;
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Submit") {
					if (($session_id = $this->model->get_session($_SESSION["risk_assess_access_code"])) == false) {
						$this->view->add_message("The session has expired. Re-enter the access code.");
						$this->view->add_tag("access_code");
						return;
					} else if ($this->model->save_vote($_POST, $session_id) == false) {
						$this->view->add_system_message("Answer both questions.");
					} else {
						$this->view->add_system_message("Choice successfully registered.");
					}
				}
			}

			$this->view->open_tag("risk");
			$this->show_options("chance", RISK_MATRIX_CHANCE);
			$this->show_options("impact", RISK_MATRIX_IMPACT);
			$this->view->close_tag();
		}
	}
?>
