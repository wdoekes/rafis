<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class case_scenarios_controller extends rafis_controller {
		private function show_overview() {
			if (($scenarios = $this->model->get_scenarios($this->case["id"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("overview");

			$this->view->open_tag("scenarios");
			foreach ($scenarios as $scenario) {
				$this->view->record($scenario, "scenario");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_scenario_form($scenario) {
			if (($actors = $this->model->get_actors()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($bia = $this->model->get_bia()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$actor_chance = config_array(ACTOR_CHANCE);
			$actor_knowledge = config_array(ACTOR_KNOWLEDGE);
			$actor_resources = config_array(ACTOR_RESOURCES);
			$actor_threat = config_array(ACTOR_THREAT_LABELS);

			if (($threats = $this->model->get_threats($this->case["id"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("edit");

			$this->view->record($scenario, "scenario");

			$this->view->open_tag("actors");
			foreach ($actors as $actor) {
				$threat_level = $this->model->actor_threat($actor);
				$actor["chance"] = $actor_chance[$actor["chance"] - 1];
				$actor["knowledge"] = $actor_knowledge[$actor["knowledge"] - 1];
				$actor["resources"] = $actor_resources[$actor["resources"] - 1];
				$actor["threat_level"] = $threat_level;
				$actor["threat"] = $actor_threat[$threat_level];

				$this->view->record($actor, "actor");
			}
			$this->view->close_tag();

			$this->view->open_tag("bia");
			foreach ($bia as $item) {
				$data = new \Banshee\message($item["impact"]);
				$data->unescaped_output();

				$item["impact"] = $data->content;
				$this->view->record($item, "item");
			}
			$this->view->close_tag();

			$this->view->open_tag("threats");
			foreach ($threats as $threat) {
				$threat["risk_value"] = $this->model->risk_matrix[$threat["chance"] - 1][$threat["impact"] - 1];
				$threat["risk_label"] = $this->model->risk_matrix_labels[$threat["risk_value"]];

				$threat["chance"] = $this->model->risk_matrix_chance[$threat["chance"] - 1];
				$threat["impact"] = $this->model->risk_matrix_impact[$threat["impact"] - 1];
				$threat["handle"] = $this->model->threat_handle_labels[$threat["handle"] - 1];

				$this->view->record($threat, "threat");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			$case_id = $this->page->parameters[0] ?? null;
			if ($this->valid_case_id($case_id) == false) {
				return;
			}

			$this->show_breadcrumbs($case_id);

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Save scenario") {
					/* Save scenario
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_scenario_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create scenario
						 */
						if ($this->model->create_scenario($_POST, $case_id) === false) {
							$this->view->add_message("Error while creating scenario.");
							$this->show_scenario_form($_POST);
						} else {
							$this->user->log_action("scenario %d created", $this->db->last_insert_id);
							$this->show_overview();
						}
					} else {
						/* Update scenario
						 */
						if ($this->model->update_scenario($_POST, $case_id) === false) {
							$this->view->add_message("Error while updating scenario.");
							$this->show_scenario_form($_POST);
						} else {
							$this->user->log_action("scenario %d updated", $_POST["id"]);
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == "Delete scenario") {
					/* Delete scenario
					 */
					if ($this->model->delete_scenario($_POST["id"], $case_id) === false) {
						$this->view->add_message("Error while deleting scenario.");
						$this->show_scenario_form($_POST);
					} else {
						$this->user->log_action("scenario %d deleted", $_POST["id"]);
						$this->show_overview();
					}
				} else {
					$this->show_overview();
				}
			} else if ($this->page->parameter_value(1, "new")) {
				/* New scenario
				 */
				$scenario = array();
				$this->show_scenario_form($scenario);
			} else if ($this->page->parameter_numeric(1)) {
				/* Edit scenario
				 */
				if (($scenario = $this->model->get_scenario($this->page->parameters[1], $case_id)) == false) {
					$this->view->add_tag("result", "Scenario not found.");
				} else {
					$this->show_scenario_form($scenario);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
