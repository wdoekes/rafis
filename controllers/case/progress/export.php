<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class case_progress_export_controller extends rafis_controller {
		public function execute() {
			$case_id = $this->page->parameters[0] ?? null;
			if ($this->valid_case_id($case_id) == false) {
				return;
			}

			if (($csv = $this->model->get_export_csv($this->case)) == false) {
				$this->view->add_tag("result", "Error while generating the report.");
			} else {
				$case_name = $this->generate_filename($this->case["organisation"]." - ".$this->case["name"]);
				print $csv->to_output($this->view, $case_name." - progress");
			}
		}
	}
?>
