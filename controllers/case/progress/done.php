<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class case_progress_done_controller extends rafis_controller {
		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$code = $_POST["code"];
			} else {
				$code = $this->page->parameters[0];
			}

			if (($data = $this->model->get_data($code)) == false) {
				$this->view->add_tag("result", "Ongeldige code.");
				return;
			}

			if (($task = $this->model->get_task($data["control_id"], $data["case_id"])) == false) {
				$this->view->add_tag("result", "Fout bij op halen taak.");
				return;
			}

			if ($task["done"] == YES) {
				$this->view->add_tag("result", "Deze taak is reeds afgerond.");
				return;
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$this->model->mark_as_done($data["control_id"], $data["case_id"]);
				$this->model->send_notification($data["control_id"], $data["case_id"]);
				$this->view->add_tag("result", "De taak is gereedgemeld.");
			} else {
				$this->view->open_tag("form");
				$this->view->add_tag("code", $this->page->parameters[0]);
				$this->view->record($task);
				$this->view->close_tag();
			}
		}
	}
?>
