<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class case_controls_controller extends rafis_controller {
		protected $prevent_repost = false;

		private function show_overview() {
			if (($threats = $this->model->get_case_threats($this->case["id"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("overview");

			$this->view->open_tag("threats");
			foreach ($threats as $threat) {
				$threat["risk_value"] = $this->model->risk_matrix[$threat["chance"] - 1][$threat["impact"] - 1];
				$threat["risk_label"] = $this->model->risk_matrix_labels[$threat["risk_value"]];

				$threat["chance"] = $this->model->risk_matrix_chance[$threat["chance"] - 1];
				$threat["impact"] = $this->model->risk_matrix_impact[$threat["impact"] - 1];
				$threat["handle"] = $this->model->threat_handle_labels[$threat["handle"] - 1];
				$this->view->record($threat, "threat");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_controls_form($threat_id, $selected) {
			if (($case_threat = $this->model->get_case_threat($threat_id, $this->case["id"])) == false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($standard = $this->model->get_standard($this->case["standard_id"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($controls = $this->model->get_controls_standard($this->case["standard_id"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($threats = $this->model->get_threats()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->add_javascript("case/controls.js");

			$reduces = config_array(CONTROL_REDUCES);

			$this->view->open_tag("edit");

			$this->view->open_tag("controls", array("standard" => $standard));
			foreach ($controls as $control) {
				$effective = $this->model->effective_control($control["reduce"], $case_threat["handle"]);

				$control["effective"] = show_boolean($effective);
				$control["reduce"] = $reduces[$control["reduce"]];
				$control["selected"] = show_boolean(in_array($control["id"], $selected));
				$this->view->record($control, "control");
			}
			$this->view->close_tag();

			$case_threat["risk_value"] = $this->model->risk_matrix[$case_threat["chance"] - 1][$case_threat["impact"] - 1];
			$case_threat["risk_label"] = $this->model->risk_matrix_labels[$case_threat["risk_value"]];
			$case_threat["chance"] = $this->model->risk_matrix_chance[$case_threat["chance"] - 1];
			$case_threat["impact"] = $this->model->risk_matrix_impact[$case_threat["impact"] - 1];
			$case_threat["handle"] = $this->model->threat_handle_labels[$case_threat["handle"] - 1];
			$this->view->record($case_threat, "threat");

			$this->view->open_tag("threats");
			$this->view->add_tag("threat", "-- none --", array("id" => 0));
			foreach ($threats as $threat) {
				$text = $threat["number"].". ".$threat["threat"];
				$this->view->add_tag("threat", $text, array("id" => $threat["id"]));
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			if ($this->page->ajax_request) {
				if (valid_input($this->page->parameters[0], VALIDATE_NUMBERS, VALIDATE_NONEMPTY)) {
					if (($controls = $this->model->get_controls_threat($this->page->parameters[0])) !== false) {
						foreach ($controls as $control) {
							$this->view->add_tag("control", $control["id"]);
						}
					}
				}
				return;
			}

			$case_id = $this->page->parameters[0] ?? null;
			if ($this->valid_case_id($case_id) == false) {
				return;
			}

			$this->show_breadcrumbs($case_id);

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($this->model->save_controls($_POST, $this->case) == false) {
					$this->show_controls_form($_POST["case_threat_id"], $_POST["selected"]);
				} else {
					$this->show_overview();
				}
			} else if ($this->page->parameter_numeric(1)) {
				/* Edit controls
				 */
				if (($selected = $this->model->get_selected_controls($this->page->parameters[1], $case_id)) === false) {
					$this->view->add_tag("result", "Threat not found.");
				} else {
					$this->show_controls_form($this->page->parameters[1], $selected);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
