<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class case_interests_controller extends rafis_controller {
		public function execute() {
			$case_id = $this->page->parameters[0] ?? null;
			if ($this->valid_case_id($case_id) == false) {
				return;
			}

			$this->show_breadcrumbs($case_id);

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($this->model->save_interests($_POST["interests"], $this->case["id"]) == false) {
					$this->view->add_message("Error while saving interests.");
				} else {
					$this->case["interests"] = $_POST["interests"];
				}
			}
			
			if ($this->page->parameter_value(1, "handout")) {
				if (($handout = $this->model->make_handout($this->case)) === false) {
					$this->view->add_tag("result", "Error while generating handout.");
				} else {
					$this->view->disable();
					$case_name = $this->generate_filename($this->case["organisation"]." - ".$this->case["name"]);
					$handout->Output($case_name." - handout.pdf", "I");
				}
			} else if ($this->page->parameter_value(1, "edit")) {
				$this->view->add_tag("edit", $this->case["interests"]);
			} else {
				$this->view->add_tag("overview", $this->case["interests"]);
			}
		}
	}
?>
