<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class case_progress_controller extends rafis_controller {
		private function show_controls($case_id) {
			if (isset($_GET["order"])) {
				$_SESSION["progress_order"] = $_GET["order"];
			} else if (isset($_SESSION["progress_order"]) == false) {
				$_SESSION["progress_order"] = "control";
			}

			$this->view->add_javascript("case/progress.js");

			if (($controls = $this->model->get_case_controls($case_id)) === false) {
				$this->view->add_tag("result", "Error reading controls.\n");
				return;
			}

			if (($standard = $this->model->get_standard($this->case["standard_id"])) === false) {
				$this->view->add_tag("result", "Error fetching ISO standard.\n");
				return;
			}

			if (($control_categories = $this->model->get_control_categories($this->case["standard_id"])) === false) {
				$this->view->add_tag("result", "Error fetching control categories.\n");
				return;
			}

			usort($controls, array($this->model, "sort_controls"));

			$today = strtotime("today");

			$done = $pending = $overdue = $idle = 0;
			foreach ($controls as $i => $control) {
				if (is_true($control["done"])) {
					$done++;
				} else if ($control["deadline"] == null) {
					$idle++;
				} else if ($control["deadline"] < $today) {
					$overdue++;
					$controls[$i]["overdue"] = true;
				} else {
					$pending++;
				}
			}

			$this->view->open_tag("overview");

			$total = count($controls);
			if ($total > 0) {
				$done = round(100 * $done / $total, 1);
				$overdue = round(100 * $overdue / $total, 1);
				$pending = round(100 * $pending / $total, 1);
				$idle = round(100 * $idle / $total, 1);
			} else {
				$done = $overdue = $pending = $idle = 0;
			}

			$this->view->add_tag("done", $done);
			$this->view->add_tag("overdue", $overdue);
			$this->view->add_tag("pending", $pending);
			$this->view->add_tag("idle", $idle);

			$categories = array();
			foreach ($controls as $control) {
				list($section) = explode(".", $control["number"]);
				if (isset($categories[$section]) == false) {
					$categories[$section] = array(0, 0);
				}
				if ($control["done"]) {
					$categories[$section][0]++;
				}
				$categories[$section][1]++;
			}

			$this->view->open_tag("categories");
			foreach ($categories as $key => $category) {
				$args = array("key" => $key.": ".$control_categories[$key]." (".$category[0]." / ".$category[1].")");
				$this->view->add_tag("category", round(100 * $category[0] / $category[1]), $args);
			}
			$this->view->close_tag();

			$params = array(
				"case_id"  => $case_id,
				"standard" => $standard);
			$this->view->open_tag("controls", $params);

			$main_cat_id = 0;

			foreach ($controls as $control) {
				list($id) = explode(".", $control["number"], 2);
				if ($_SESSION["progress_order"] != "control") {
					$args = array();
				} else if ($id != $main_cat_id) {
					$args = array("category" => $control_categories[$id]);
					$main_cat_id = $id;
				} else {
					$args = array();
				}

				$control["overdue"] = show_boolean($control["overdue"] ?? false);
				if ($control["deadline"] != null) {
					$control["deadline"] = date("j M Y", $control["deadline"]);
				}
				$control["check"] = $control["done"] ? "green" : "grey";
				$control["done"] = show_boolean($control["done"]);
				$this->view->record($control, "control", $args);
			}

			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function edit_progress($case_id, $progress) {
			if (($people = $this->model->get_people()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($control = $this->model->get_control($progress["control_id"])) === false) {
				$this->view->add_tag("result", "Voortgang niet gevonden.");
				return;
			}

			if (($threats = $this->model->get_case_threats($progress["control_id"], $case_id)) === false) {
				$this->view->add_tag("result", "Database error!");
				return;
			}

			$this->view->add_javascript("banshee/datepicker.js");

			$this->view->open_tag("edit");

			$this->view->open_tag("threats");
			foreach ($threats as $threat) {
				$threat["handle"] = $this->model->threat_handle_labels[$threat["handle"] - 1];
				$this->view->record($threat, "threat");
			}
			$this->view->close_tag();

			$this->view->open_tag("people");
			$this->view->record(array("name" => ""), "person", array("id" => 0));
			foreach ($people as $person) {
				$this->view->record($person, "person");
			}
			$this->view->close_tag();

			$this->view->open_tag("progress");
			$this->view->add_tag("case_id", $case_id);
			$this->view->add_tag("executor_id", $progress["executor_id"] ?? "");
			$this->view->add_tag("reviewer_id", $progress["reviewer_id"] ?? "");
			$this->view->add_tag("control_id", $progress["control_id"]);
			$this->view->add_tag("control", sprintf("%s %s", $control["number"], $control["name"]));
			$this->view->add_tag("deadline", $progress["deadline"] ?? "");
			$this->view->add_tag("info", $progress["info"] ?? "");
			$this->view->add_tag("done", show_boolean($progress["done"] ?? null));
			$this->view->add_tag("hours_planned", $progress["hours_planned"] ?? 0);
			$this->view->add_tag("hours_invested", $progress["hours_invested"] ?? 0);
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			$case_id = $this->page->parameters[0] ?? null;
			if ($this->valid_case_id($case_id) == false) {
				return;
			}

			$this->show_breadcrumbs($case_id);

			$this->view->add_javascript("webui/jquery-ui.js");
			$this->view->add_css("webui/jquery-ui.css");

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				/* Save progress
				 */
				if ($this->model->progress_oke($_POST, $case_id) == false) {
					$this->edit_progress($case_id, $_POST);
				} else if ($this->model->save_progress($_POST, $case_id) == false) {
					$this->view->add_message("Error while saving progress.");
					$this->edit_progress($case_id, $_POST);
				} else {
					if ($_POST["submit_button"] == "Save with notification") {
						$this->model->send_notifications($_POST, $case_id);
					}
					$this->show_controls($case_id);
				}
			} else if ($this->page->parameter_numeric(1)) {
				if (($progress = $this->model->get_progress($this->page->parameters[1], $case_id)) === false) {
					/* Progress not found
					 */
					$this->view->add_tag("result", "Database error.");
				} else {
					/* Progress form
					 */
					$this->edit_progress($case_id, $progress);
				}
			} else {
				/* Show overview
				 */
				$this->show_controls($case_id);
			}
		}
	}
?>
