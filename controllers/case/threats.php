<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class case_threats_controller extends rafis_controller {
		private function show_overview() {
			if (($threats = $this->model->get_case_threats($this->case["id"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("overview");

			$this->view->open_tag("threats");
			foreach ($threats as $threat) {
				$threat["chance"] = $this->model->risk_matrix_chance[$threat["chance"] - 1];
				$threat["impact"] = $this->model->risk_matrix_impact[$threat["impact"] - 1];
				$threat["handle"] = $this->model->threat_handle_labels[$threat["handle"] - 1];
				$this->view->record($threat, "threat");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_threat_template_form() {
			if (($templates = $this->model->get_threat_templates()) == false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($categories = $this->model->get_categories()) == false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$category_id = 0;

			$this->view->open_tag("templates");
			foreach ($templates as $template) {
				if ($template["category_id"] != $category_id) {
					$category_id = $template["category_id"];
					$template["category"] = $categories[$category_id]["name"];
				}
				$this->view->record($template, "template");
			}
			$this->view->close_tag();
		}

		private function show_threat_form($threat) {
			$cia = array("confidentiality", "integrity", "availability");

			if (isset($threat["template_id"])) {
				if (($template = $this->model->get_threat_template($threat["template_id"], $this->case["standard_id"])) != false) {
					foreach ($cia as $s) {
						if ($template[$s] == "") {
							$threat[$s] = "-";
						} else {
							$threat[$s] = $template[$s];
						}
					}

					$threat["template"] = $template["threat"];
					$threat["description"] = $template["description"];
					$threat["controls"] = $template["controls"];
					$threat["controls"]["standard"] = $template["standard"];
				}
			}

			if (($scope = $this->model->get_scope($this->case["id"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($actors = $this->model->get_actors()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("edit");

			/* Actors
			 */
			$this->view->open_tag("actors");
			$this->view->add_tag("actor", "", array("id" => 0));
			foreach ($actors as $actor) {
				$label = $actor["name"]." - Threat level: ".$actor["threat"];
				$this->view->add_tag("actor", $label, array("id" => $actor["id"]));
			}
			$this->view->close_tag();

			/* Chance values
			 */
			$this->view->open_tag("chance");
			$this->view->add_tag("option", "", array("value" => 0));
			foreach ($this->model->risk_matrix_chance as $value => $label) {
				$this->view->add_tag("option", $label, array("value" => $value + 1));
			}
			$this->view->close_tag();

			/* Impact values
			 */
			$this->view->open_tag("impact");
			$this->view->add_tag("option", "", array("value" => 0));
			foreach ($this->model->risk_matrix_impact as $value => $label) {
				$this->view->add_tag("option", $label, array("value" => $value + 1));
			}
			$this->view->close_tag();

			/* Handle values
			 */
			$this->view->open_tag("handle");
			$this->view->add_tag("option", "", array("value" => "0"));
			foreach ($this->model->threat_handle_labels as $i => $option) {
				$this->view->add_tag("option", $option, array("value" => $i + 1));
			}
			$this->view->close_tag();

			/* Scope
			 */
			if (is_array($threat["threat_scope"] ?? false) == false) {
				$threat["threat_scope"] = array();
			}

			$this->view->open_tag("scope");
			foreach ($scope as $system) {
				$system["confidentiality"] = $this->model->confidentiality_score[$system["confidentiality"] - 1] ?? "";
				$system["integrity"] = $this->model->integrity_score[$system["integrity"] - 1] ?? "";
				$system["availability"] = $this->model->availability_score[$system["availability"] - 1] ?? "";
				$system["selected"] = show_boolean(in_array($system["id"], $threat["threat_scope"]));

				$this->view->record($system, "system");
			}
			$this->view->close_tag();

			$this->view->record($threat, "threat", array(), true);

			$this->view->close_tag();
		}

		public function execute() {
			$case_id = $this->page->parameters[0] ?? null;
			if ($this->valid_case_id($case_id) == false) {
				return;
			}

			$this->show_breadcrumbs($case_id);

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Save threat") {
					/* Save threat
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_threat_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create threat
						 */
						if ($this->model->create_threat($_POST, $this->case) === false) {
							$this->view->add_message("Error creating threat.");
							$this->show_threat_form($_POST);
						} else {
							$this->user->log_action("threat %d created", $this->db->last_insert_id);
							$this->show_overview();
						}
					} else {
						/* Update threat
						 */
						if ($this->model->update_threat($_POST, $case_id) === false) {
							$this->view->add_message("Error updating threat.");
							$this->show_threat_form($_POST);
						} else {
							$this->user->log_action("threat %d updated", $_POST["id"]);
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == "Delete threat") {
					/* Delete threat
					 */
					if ($this->model->delete_oke($_POST) == false) {
						$this->show_threat_form($_POST);
					} else if ($this->model->delete_threat($_POST["id"], $case_id) === false) {
						$this->view->add_message("Error deleting threat.");
						$this->show_threat_form($_POST);
					} else {
						$this->user->log_action("threat %d deleted", $_POST["id"]);
						$this->show_overview();
					}
				} else {
					$this->show_overview();
				}
			} else if ($this->page->parameter_value(1, "template")) {
				$this->show_threat_template_form();
			} else if ($this->page->parameter_value(1, "new")) {
				/* New threat
				 */
				$threat = array();
				if ($this->page->parameter_numeric(2)) {
					$threat["template_id"] = $this->page->parameters[2];
				}
				$this->show_threat_form($threat);
			} else if ($this->page->parameter_numeric(1)) {
				/* Edit threat
				 */
				if (($threat = $this->model->get_case_threat($this->page->parameters[1], $case_id)) == false) {
					$this->view->add_tag("result", "Threat not found.");
				} else {
					$this->show_threat_form($threat);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
