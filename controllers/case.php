<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class case_controller extends rafis_controller {
		private function show_overview($archive = null) {
			if ($archive === null) {
				$archive = $_SESSION["case_archive"];
			} else {
				$_SESSION["case_archive"] = $archive;
			}

			if (($cases = $this->model->get_cases($archive)) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$attr = array("archive" => show_boolean($archive));

			if ($archive) {
				$this->view->add_javascript("case.js");
				$this->view->run_javascript("archive()");
			} else {
				$attr["archived"] = $this->model->count_archive();
			}

			$this->view->open_tag("overview", $attr);

			foreach ($cases as $case) {
				$case["date"] = date_string("j M Y", $case["date"]);
				$case["start"] = $this->model->start_crumb($case);
				$this->view->record($case, "case");
			}

			$this->view->close_tag();
		}

		private function show_case_form($case) {
			if (($standards = $this->model->get_standards()) == false) {
				$this->view->add_tag("result", "Error retrieving standards.");
				return;
			}

			$this->view->add_javascript("webui/jquery-ui.js");
			$this->view->add_javascript("banshee/datepicker.js");
			$this->view->add_javascript("banshee/datepicker-nl.js");

			$this->view->add_css("webui/jquery-ui.css");

			$this->view->open_tag("edit");

			$this->view->open_tag("standards");
			foreach ($standards as $standard) {
				$this->view->add_tag("standard", $standard["name"], array("id" => $standard["id"]));
			}
			$this->view->close_tag();

			if (($impact = json_decode($case["impact"], true)) == null) {
				$impact = array();
			}
			unset($case["impact"]);

			$case["archived"] = show_boolean($case["archived"] ?? false);
			$this->view->record($case, "case");

			$this->view->open_tag("impact");
			foreach ($this->model->risk_matrix_impact as $i => $label) {
				$label[0] = strtoupper($label[0]);
				$this->view->add_tag("value", $impact[$i], array("label" => $label));
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			$this->view->title = "Casussen";

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Save case") {
					if (is_array($_POST["impact"]) == false) {
						$_POST["impact"] = array();
					} else {
						$_POST["impact"] = array_values($_POST["impact"]);
						$_POST["impact"] = array_slice($_POST["impact"], 0, 5);
					}
					$_POST["impact"] = json_encode($_POST["impact"]);

					/* Save case
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_case_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create case
						 */
						if ($this->model->create_case($_POST) == false) {
							$this->view->add_message("Error while creating case.");
							$this->show_case_form($_POST);
						} else {
							$this->user->log_action("case created");
							$this->show_overview();
						}
					} else {
						/* Update case
						 */
						if ($this->valid_case_id($_POST["id"]) == false) {
							$this->view->add_message("Invalid case id.");
							$this->show_case_form($_POST);
						} else if ($this->model->update_case($_POST) === false) {
							$this->view->add_message("Error while updating case.");
							$this->show_case_form($_POST);
						} else {
							$this->user->log_action("case updated");
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == "Delete case") {
					/* Delete case
					 */
					if ($this->valid_case_id($_POST["id"]) == false) {
						$this->view->add_message("Invalid case id.");
						$this->show_case_form($_POST);
					} else if ($this->model->delete_case($_POST["id"]) == false) {
						$this->view->add_message("Error while deleting case.");
						$this->show_case_form($_POST);
					} else {
						$this->user->log_action("case deleted");
						$this->show_overview();
					}
				} else {
					$this->show_overview();
				}
			} else if (($this->page->parameters[0] ?? null) == "new") {
				/* New case
				 */
				$impact = str_replace("{E}", EURO, DEFAULT_IMPACT);
				$impact = json_encode(config_array($impact, false));
				$case = array(
					"standard_id"  => $this->settings->default_standard,
					"date"         => date("Y-m-d"),
					"impact"       => $impact);
				$this->show_case_form($case);
			} else if (($this->page->parameters[0] ?? null) == "archive") {
				$this->show_overview(true);
			} else if (valid_input($this->page->parameters[0] ?? null, VALIDATE_NUMBERS, VALIDATE_NONEMPTY)) {
				/* Edit case
				 */
				if (($case = $this->model->get_case($this->page->parameters[0])) === false) {
					$this->view->add_tag("result", "Case not found.\n");
				} else {
					$this->show_case_form($case);
				}
			} else if (($this->page->parameters[0] ?? null) != null) {
				/* Unknown (sub)module
				 */
				$this->view->add_tag("result", "Unknown module", array("page" => "casus"));
			} else {
				/* Show overview
				 */
				$this->show_overview(false);
			}
		}
	}
?>
