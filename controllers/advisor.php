<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class advisor_controller extends Banshee\controller {
		protected $prevent_repost = false;

		private function overview() {
			if (($roles = $this->model->get_roles()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->add_help_button();

			$this->view->open_tag("overview");
			foreach ($roles as $role) {
				if ($role["organisation_id"] == ($_SESSION["advisor_organisation_id"] ?? null)) {
					$ready = "active";
				} else {
					$ready = show_boolean($role["crypto_key"] != null);
				}

				$attr = array(
					"id"    => $role["id"],
					"ready" => $ready);
				$this->view->add_tag("role", $role["name"], $attr);
			}
			$this->view->close_tag();
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Request") {
					if ($this->model->request_adviser_role($_POST["user"])) {
						$this->view->add_system_message("Your request has been sent.");
					}
				} else if ($_POST["submit_button"] == "Activate") {
					if ($this->model->activate_role($_POST["id"])) {
						$this->view->add_message("Role activated.");
					}
				} else if ($_POST["submit_button"] == "Deactivate") {
					$this->model->deactivate_role();
					$this->view->add_message("Role deactivated.");
				} else if ($_POST["submit_button"] == "Remove") {
					if ($_SESSION["advisor_organisation_id"] == $_POST["id"]) {
						$this->model->deactivate_role();
						$this->view->add_system_message("Role deactivated.");
						$this->user->log_action("advisor role deactivated");
					}
					if ($this->model->delete_role($_POST["id"])) {
						$this->view->add_system_message("Role removed.");
					}
				}
			}

			$this->overview();
		}
	}
?>
