<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class bia_controller extends rafis_controller {
		private function show_overview() {
			if (($items = $this->model->get_items()) === false) {
				$this->view->add_tag("result", "Database error.", array("url" => "dashboard"));
				return;
			}

			$this->view->open_tag("overview");
			foreach ($items as $item) {
				$item["availability"] = $this->model->availability_score[$item["availability"] - 1] ?? "";
				$item["confidentiality"] = $this->model->confidentiality_score[$item["confidentiality"] - 1] ?? "";
				$item["integrity"] = $this->model->integrity_score[$item["integrity"] - 1] ?? "";
				$item["value"] = $this->model->asset_value_labels[$item["value"]];
				$item["owner"] = show_boolean($item["owner"]);
				$item["personal_data"] = show_boolean($item["personal_data"]);

				$this->view->record($item, "item");
			}
			$this->view->close_tag();
		}

		private function show_bia_form($item) {
			$this->view->open_tag("edit");

			$this->view->open_tag("availability");
			foreach ($this->model->availability_score as $value => $label) {
				$this->view->add_tag("label", $label, array("value" => $value + 1));
			}
			$this->view->close_tag();

			$this->view->open_tag("confidentiality");
			foreach ($this->model->confidentiality_score as $value => $label) {
				$this->view->add_tag("label", $label, array("value" => $value + 1));
			}
			$this->view->close_tag();

			$this->view->open_tag("integrity");
			foreach ($this->model->integrity_score as $value => $label) {
				$this->view->add_tag("label", $label, array("value" => $value + 1));
			}
			$this->view->close_tag();

			$locations = config_array(BIA_LOCATIONS);
			$this->view->open_tag("location");
			foreach ($locations as $value => $label) {
				$this->view->add_tag("label", $label, array("value" => $value));
			}
			$this->view->close_tag();

			$item["owner"] = show_boolean($item["owner"] ?? false);
			$item["personal_data"] = show_boolean($item["personal_data"] ?? false);

			$this->view->record($item, "item");

			$this->view->close_tag();
		}

		public function execute() {
			$this->view->add_help_button();

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Save information system") {
					/* Save item
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_bia_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create item
						 */
						if ($this->model->create_item($_POST) === false) {
							$this->view->add_message("Error creating information system.");
							$this->show_bia_form($_POST);
						} else {
							$this->user->log_action("bia created");
							$this->show_overview();
						}
					} else {
						/* Update item
						 */
						if ($this->model->update_item($_POST) === false) {
							$this->view->add_message("Error updating information system.");
							$this->show_bia_form($_POST);
						} else {
							$this->user->log_action("bia updated");
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == "Delete information system") {
					/* Delete item
					 */
					if ($this->model->delete_oke($_POST["id"]) == false) {
						$this->show_bia_form($_POST);
					} else if ($this->model->delete_item($_POST["id"]) == false) {
						$this->view->add_message("Error deleting information system.");
						$this->show_bia_form($_POST);
					} else {
						$this->user->log_action("bia deleted");
						$this->show_overview();
					}
				} else {
					$this->show_overview();
				}
			} else if ($this->page->parameter_value(0, "new")) {
				/* New item
				 */
				$item = array();
				$this->show_bia_form($item);
			} else if ($this->page->parameter_numeric(0)) {
				/* Edit item
				 */
				if (($item = $this->model->get_item($this->page->parameters[0])) === false) {
					$this->view->add_tag("result", "Information system not found.\n");
				} else {
					$this->show_bia_form($item);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
