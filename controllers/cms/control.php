<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class cms_control_controller extends Banshee\controller {
		private function show_overview() {
			if (($control_count = $this->model->count_controls($_SESSION["standard"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($categories = $this->model->get_categories($_SESSION["standard"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$paging = new \Banshee\pagination($this->view, "controls", $this->settings->admin_page_size, $control_count);

			if (($controls = $this->model->get_controls($_SESSION["standard"], $paging->offset, $paging->size)) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$reduce = config_array(CONTROL_REDUCES);

			$this->view->open_tag("overview");

			$category_id = 0;
			$this->view->open_tag("controls");
			foreach ($controls as $control) {
				list($major) = explode(".", $control["number"]);
				if ($major != $category_id) {
					$category_id = $major;
					$control["category"] = $categories[$category_id]["name"];
				}
				$control["reduce"] = $reduce[$control["reduce"]];
				$this->view->record($control, "control");
			}
			$this->view->close_tag();

			$paging->show_browse_links();

			$this->view->close_tag();
		}

		private function show_control_form($control) {
			if (($threats = $this->model->get_threats()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (is_array($measure["threat_links"] ?? null) == false) {
				$measure["threat_links"] = array();
			}

			$this->view->open_tag("edit");

			$this->view->record($control, "control");

			$this->view->open_tag("reduces");
			foreach (config_array(CONTROL_REDUCES) as $reduce) {
				$this->view->add_tag("reduce", $reduce);
			}
			$this->view->close_tag();

			$this->view->open_tag("threats");
			foreach ($threats as $threat) {
				$params = array(
					"id"      => $threat["id"],
					"checked" => show_boolean(in_array($threat["id"], $control["threat_links"])));
				$this->view->open_tag("threat", $params);
				$this->view->add_tag("number", $threat["number"]);
				$this->view->add_tag("title", $threat["number"].". ".$threat["threat"]);
				$this->view->close_tag();
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			$this->view->add_css("includes/standard.css");

			if (($standard = $this->model->get_standard($_SESSION["standard"])) != false) {
				$this->view->add_tag("standard", $standard["name"]);
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Save control") {
					/* Save control
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_control_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create control
						 */
						if ($this->model->create_control($_POST, $_SESSION["standard"]) === false) {
							$this->view->add_message("Error creating control.");
							$this->show_control_form($_POST);
						} else {
							$this->user->log_action("control created");
							$this->show_overview();
						}
					} else {
						/* Update control
						 */
						if ($this->model->update_control($_POST, $_SESSION["standard"]) === false) {
							$this->view->add_message("Error updating control.");
							$this->show_control_form($_POST);
						} else {
							$this->user->log_action("control updated");
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == "Delete control") {
					/* Delete control
					 */
					if ($this->model->delete_oke($_POST) == false) {
						$this->show_control_form($_POST);
					} else if ($this->model->delete_control($_POST["id"]) === false) {
						$this->view->add_message("Error deleting control.");
						$this->show_control_form($_POST);
					} else {
						$this->user->log_action("control deleted");
						$this->show_overview();
					}
				} else {
					$this->show_overview();
				}
			} else if ($this->page->parameter_value(0, "new")) {
				/* New control
				 */
				$control = array("threat_links" => array());
				$this->show_control_form($control);
			} else if ($this->page->parameter_numeric(0)) {
				/* Edit control
				 */
				if (($control = $this->model->get_control($this->page->parameters[0])) === false) {
					$this->view->add_tag("result", "Case not found.\n");
				} else {
					$this->show_control_form($control);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
