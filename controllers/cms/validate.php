<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class cms_validate_controller extends Banshee\controller {
		public function execute() {
			$this->view->add_css("includes/standard.css");

			if (($standard = $this->model->get_standard($_SESSION["standard"])) != false) {
				$this->view->add_tag("standard", $standard["name"]);
			}

			if (($threats = $this->model->linked_threats($_SESSION["standard"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($controls = $this->model->linked_controls($_SESSION["standard"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("validate");

			$this->view->open_tag("threats");
			foreach ($threats as $threat) {
				$this->view->record($threat, "threat");
			}
			$this->view->close_tag();

			$this->view->open_tag("controls");
			foreach ($controls as $control) {
				$this->view->record($control, "control");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}
	}
?>
