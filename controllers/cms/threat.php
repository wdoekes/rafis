<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class cms_threat_controller extends Banshee\controller {
		private function show_overview() {
			if (($threat_count = $this->model->count_threats()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($categories = $this->model->get_categories()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$paging = new \Banshee\pagination($this->view, "admin_threats", $this->settings->admin_page_size, $threat_count);

			if (($threats = $this->model->get_threats($_SESSION["standard"], $paging->offset, $paging->size)) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("overview");

			$category_id = 0;

			$this->view->open_tag("threats");
			foreach ($threats as $threat) {
				if ($threat["category_id"] != $category_id) {	
					$category_id = $threat["category_id"];
					$threat["category"] = $categories[$category_id]["name"];
				}
				$this->view->record($threat, "threat");
			}
			$this->view->close_tag();

			$paging->show_browse_links();

			$this->view->close_tag();
		}

		private function show_threat_form($threat) {
			if (($categories = $this->model->get_categories()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($controls = $this->model->get_controls($_SESSION["standard"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (is_array($threat["links"] ?? null) == false) {
				$threat["links"] = array();
			}

			$this->view->open_tag("edit");

			$this->view->record($threat, "threat");

			$this->view->open_tag("categories");
			foreach ($categories as $category) {
				$this->view->add_tag("category", $category["name"], array("id" => $category["id"]));
			}
			$this->view->close_tag();

			$this->view->open_tag("controls");
			foreach ($controls as $control) {
				$params = array(
					"id"      => $control["id"],
					"checked" => show_boolean(in_array($control["id"], $threat["links"])));
				$this->view->open_tag("control", $params);
				$this->view->add_tag("title", $control["number"]." ".$control["name"]);
				$this->view->add_tag("description", $control["description"] ?? "");
				$this->view->close_tag();
			}
			$this->view->close_tag();

			$this->view->open_tag("cia");
			$this->view->add_tag("option", "-");
			$this->view->add_tag("option", "p");
			$this->view->add_tag("option", "s");
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			$this->view->add_css("includes/standard.css");

			if (($standard = $this->model->get_standard($_SESSION["standard"])) != false) {
				$this->view->add_tag("standard", $standard["name"]);
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Save threat") {
					/* Save threat
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_threat_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create threat
						 */
						if ($this->model->create_threat($_POST) === false) {
							$this->view->add_message("Error creating threat.");
							$this->show_threat_form($_POST);
						} else {
							$this->user->log_action("threat created");
							$this->view->remove_from_cache("koppelingen");
							header("X-Hiawatha-Cache-Remove: /koppelingen");
							$this->show_overview();
						}
					} else {
						/* Update threat
						 */
						if ($this->model->update_threat($_POST, $_SESSION["standard"]) === false) {
							$this->view->add_message("Error updating threat.");
							$this->show_threat_form($_POST);
						} else {
							$this->user->log_action("threat updated");
							$this->view->remove_from_cache("koppelingen");
							header("X-Hiawatha-Cache-Remove: /koppelingen");
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == "Delete threat") {
					/* Delete threat
					 */
					if ($this->model->delete_oke($_POST) == false) {
						$this->show_threat_form($_POST);
					} else if ($this->model->delete_threat($_POST["id"]) === false) {
						$this->view->add_message("Error deleting threat.");
						$this->show_threat_form($_POST);
					} else {
						$this->user->log_action("threat deleted");
						$this->view->remove_from_cache("koppelingen");
						header("X-Hiawatha-Cache-Remove: /koppelingen");
						$this->show_overview();
					}
				} else {
					$this->show_overview();
				}
			} else if ($this->page->parameter_value(0, "new")) {
				/* New threat
				 */
				$threat = array("links" => array());
				$this->show_threat_form($threat);
			} else if ($this->page->parameter_numeric(0)) {
				/* Edit threat
				 */
				if (($threat = $this->model->get_threat($this->page->parameters[0])) === false) {
					$this->view->add_tag("result", "Threat not found.\n");
				} else {
					$this->show_threat_form($threat);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
