<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class links_controller extends Banshee\controller {
		public function execute() {
			if (isset($_SESSION["standard"]) == false) {
				$_SESSION["standard"] = $this->settings->default_standard;
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$_SESSION["standard"] = $_POST["standard"];
			}

			$this->view->title = "Links between threats and controls";
			$this->view->keywords = "links";

			$this->view->add_javascript("links.js");

			if (($standards = $this->model->get_standards()) == false) {
				$this->view->add_tag("result", "Error retrieving standards.");
				return;
			}

			if (($categories = $this->model->get_threat_categories()) == false) {
				$this->view->add_tag("result", "Error getting threat categories.");
				return;
			}

			if (($threats = $this->model->get_threats()) == false) {
				$this->view->add_tag("result", "Error getting threats.");
				return;
			}

			if (($controls = $this->model->get_controls($_SESSION["standard"])) === false) {
				$this->view->add_tag("result", "Error getting ISO controls.");
				return;
			}

			if (($control_categories = $this->model->get_control_categories($_SESSION["standard"])) === false) {
				$this->view->add_tag("result", "Error getting ISO control categories.");
				return;
			}

			if (($mitigation = $this->model->get_mitigation($_SESSION["standard"])) === false) {
				$this->view->add_tag("result", "Error getting mitigation.");
				return;
			}

			$this->view->open_tag("links");

			/* Standards
			 */
			$this->view->open_tag("standards");
			foreach ($standards as $standard) {
				$params = array(
					"id"       => $standard["id"],
					"selected" => show_boolean($standard["id"] == $_SESSION["standard"]));
				$this->view->add_tag("standard", $standard["name"], $params);
			}
			$this->view->close_tag();

			/* Threats
			 */
			$links = array();
			foreach ($mitigation as $rule) {
				if (is_array($links[$rule["threat_id"]] ?? null) == false) {
					$links[$rule["threat_id"]] = array();
				}
				array_push($links[$rule["threat_id"]], $rule["control_id"]);
			}

			$category_id = 0;
			$this->view->open_tag("threats");
			foreach ($threats as $threat) {
				$this->view->open_tag("threat", array("id" => $threat["id"]));
				$this->view->add_tag("number", $threat["number"]);
				$this->view->add_tag("threat", $threat["threat"]);
				$this->view->add_tag("description", $threat["description"]);
				if (is_array($links[$threat["id"]])) {
					foreach ($links[$threat["id"]] as $control_id) {
						$control = $controls[$control_id];
						$this->view->add_tag("control", $control["number"]." ".$control["name"]);
					}
				}
				$this->view->add_tag("confidentiality", $threat["confidentiality"]);
				$this->view->add_tag("integrity", $threat["integrity"]);
				$this->view->add_tag("availability", $threat["availability"]);
				if ($threat["category_id"] != $category_id) {
					$category_id = $threat["category_id"];
					$this->view->add_tag("category", $categories[$category_id]);
				}
				$this->view->close_tag();

			}
			$this->view->close_tag();

			/* ISO controls
			 */
			$links = array();
			foreach ($mitigation as $rule) {
				if (is_array($links[$rule["control_id"]] ?? null) == false) {
					$links[$rule["control_id"]] = array();
				}
				array_push($links[$rule["control_id"]], $rule["threat_id"]);
			}

			$reduce = config_array(CONTROL_REDUCES);

			$category_id = 0;
			$this->view->open_tag("controls");
			foreach ($controls as $control) {
				$this->view->open_tag("control", array("id" => $control["id"]));
				$this->view->add_tag("number", $control["number"]);
				$this->view->add_tag("control", $control["name"]);
				$this->view->add_tag("reduce", $reduce[$control["reduce"]]);

				list($major) = explode(".", $control["number"], 2);
				if ($major != $category_id) {
					$category_id = $major;
					$this->view->add_tag("category", $control_categories[$category_id]["name"]);
				}

				if (is_array($links[$control["id"]] ?? null)) {
					$linked_threats = array();
					foreach ($links[$control["id"]] as $threat_id) {
						$threat = $threats[$threat_id];
						$linked_threats[$threat["number"]] = $threat["threat"];
					}
					ksort($linked_threats);
					foreach ($linked_threats as $number => $threat) {
						$this->view->add_tag("threat", $number.". ".$threat);
					}
				}
				$this->view->close_tag();
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}
	}
?>
