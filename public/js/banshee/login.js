$(document).ready(function() {
	var username = document.getElementById('username');
	var password = document.getElementById('password');

	if ($('input#username').val() == '') {
		username.focus();
	} else {
		password.focus();
	}
});

function login_demo() {
	$('input#username').val('demo');
	$('input#password').val('demo');
	$('form#login input[type="submit"]').click();
}
