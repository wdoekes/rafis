function enable_form() {
	$('.hidden').addClass('not_hidden');
	$('.hidden').removeClass('hidden');

	$('.visible').addClass('hidden');
}

function disable_form() {
	$('.visible').removeClass('hidden');

	$('.not_hidden').addClass('hidden');
}
