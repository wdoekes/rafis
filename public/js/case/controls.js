function highlight(option) {
	$('table.controls td').css('background-color', '#ffffff');

	var threat_id = $(option).val();
	if (threat_id > 0) {
		$.get('/case/controls/'+threat_id, function(data) {
			$(data).find('control').each(function() {
				var control_id = $(this).text();
				$('table.controls tr.control_'+control_id+' td').css('background-color', '#ffffd0');
			});
		});
	}
}
