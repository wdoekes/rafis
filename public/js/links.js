function show_threats() {
	$("#threat").addClass("selected");
	$("#control").removeClass("selected");

	$(".threats").show();
	$(".controls").hide();
}

function show_controls() {
	$("#threat").removeClass("selected");
	$("#control").addClass("selected");

	$(".threats").hide();
	$(".controls").show();
}
