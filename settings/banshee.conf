#------------------------------------------------------------------------------
#
#  Banshee's general website settings
#
#------------------------------------------------------------------------------

# Database settings
#
DB_HOSTNAME = localhost
DB_DATABASE = rafis
DB_USERNAME = rafis
DB_PASSWORD = rafis

# Specify what kind of files can be uploaded via the File Administration page.
#
ALLOWED_UPLOADS = jpg|jpeg|gif|png|ico|pdf|doc|docx|xls|xlsx|txt|rtf|odt|ods|odp

# The format of the API communication. Use 'xml' or 'json'.
#
API_OUTPUT_TYPE = xml

# Default lifetime of the data in the internal cache.
#
CACHE_TIMEOUT = 86400

# Names of the days and months to be used in date_string().
#
MONTHS_OF_YEAR = january|february|march|april|may|june| \
                 july|august|september|october|november|december
DAYS_OF_WEEK = monday|tuesday|wednesday|thursday|friday|saterday|sunday

# Print error message on the screen or send them via e-mail to the administrator.
#
DEBUG_MODE = yes

# Redirect HTTP requests to HTTPS.
#
ENFORCE_HTTPS = no

# Load extra config files from the settings directoy. Omit the .conf extension.
#
EXTRA_SETTINGS = rafis

# Security Headers
#
HEADER_CSP = default-src 'self' 'unsafe-inline' 'unsafe-eval'; img-src * data:
HEADER_FP = camera 'none'; geolocation 'none'; microphone 'none'

# Hide menu for unauthorized users.
#
HIDE_MENU_FOR_VISITORS = yes

# The layout to be used for the website and the CMS. Layouts can be found in
# view/banshee/layout_*.xslt. The corresponding stylesheets are located in
# public/css/banshee/layout_*.css.
#
LAYOUT_CMS = cms
LAYOUT_SITE = rafis

# Log all database queries to logfiles/database.log.
#
LOG_DB_QUERIES = no

# Only show menu items the current user can access.
#
MENU_PERSONALIZED = no

# Enable multilingual capabilities.
#
MULTILINGUAL = no

# The languages you can chose from in the Page Administration page.
#
SUPPORTED_LANGUAGES = en:English

# This setting enables two-factor authentication. This requires the usage
# of a mobile app that implements RFC 6238, uses BASE32 characters, sha1,
# a 30 seconds time interval and produces a 6 digit code.
#
USE_AUTHENTICATOR = no

# The domain name of this website. Will be used by the analytics module.
#
WEBSITE_DOMAIN = rafis.eu

# Off line mode disables all modules and features of this website and shows a message.
# When set to an IP-address, only computers behind that IP-address will have normal access.
#
WEBSITE_ONLINE = yes
