<?php
	class bia_model extends rafis_model {
		private function sort_bia($bia1, $bia2) {
			return strcmp($bia1["item"], $bia2["item"]);
		}

		public function get_items() {
			$query = "select * from bia where organisation_id=%d";

			if (($items = $this->db->execute($query, $this->organisation_id)) === false) {
				return false;
			}

			foreach (array_keys($items) as $key) {
				$this->decrypt($items[$key], "item", "description", "impact");

				$item = $items[$key];
				$a = $item["availability"] - 1;
				$i = $item["integrity"] - 1;
				$c = $item["confidentiality"] - 1;
				$items[$key]["value"] = $this->asset_value[$i][$c][$a] ?? 0;
			}

			uasort($items, array($this, "sort_bia"));

			return $items;
		}

		public function get_item($item_id) {
			$query = "select * from bia where id=%d and organisation_id=%d limit 1";

			if (($result = $this->db->execute($query, $item_id, $this->organisation_id)) == false) {
				return false;
			}

			$this->decrypt($result[0], "item", "description", "impact");

			return $result[0];
		}

		public function save_oke($item) {
			$result = true;

			if (trim($item["item"]) == "") {
				$this->view->add_message("Enter the name of the information system.");
				$result = false;
			} else if (strlen($item["item"]) > 150) {
				$this->view->add_message("Information system name is too long. Maximum of 150 characters.");
				$result = false;
			}

			return $result;
		}

		public function create_item($item) {
			$keys = array("id", "organisation_id", "item", "description", "impact", "availability", "integrity", "confidentiality", "owner", "personal_data", "location");

			$item["id"] = null;
			$item["organisation_id"] = $this->organisation_id;
			$item["owner"] = is_true($item["owner"] ?? null) ? YES : NO;
			$item["personal_data"] = is_true($item["personal_data"] ?? null) ? YES : NO;

			$this->encrypt($item, "item", "description", "impact");

			return $this->db->insert("bia", $item, $keys);
		}

		public function update_item($item) {
			if ($this->get_item($item["id"]) == false) {
				return false;
			}

			$keys = array("item", "description", "impact", "availability", "integrity", "confidentiality", "owner", "personal_data", "location");

			$item["owner"] = is_true($item["owner"] ?? null) ? YES : NO;
			$item["personal_data"] = is_true($item["personal_data"] ?? null) ? YES : NO;

			$this->encrypt($item, "item", "description", "impact");

			return $this->db->update("bia", $item["id"], $item, $keys);
		}

		public function delete_oke($item_id) {
			$query = "select count(*) as count from case_scope s, cases c ".
			         "where s.bia_id=%d and s.case_id=c.id and c.organisation_id=%d";

			if (($result = $this->db->execute($query, $item_id, $this->user->organisation_id)) == false) {
				$this->view->add_message("Database error.");
				return false;
			}

			if ($result[0]["count"] > 0) {
				$this->view->add_message("This information system lies within the scope of %d risk analyses.", $result[0]["count"]);
				return false;
			}

			return true;
		}

		public function delete_item($item_id) {
			if ($this->get_item($item_id) == false) {
				return false;
			}

			$queries = array(
				array("delete from case_threat_bia where bia_id=%d", $item_id),
				array("delete from bia where id=%d", $item_id));

			return $this->db->transaction($queries);
		}
	}
?>
