<?php
	class case_model extends rafis_model {
		public function get_cases($archive) {
			$query = "select c.id, c.name, c.organisation, c.standard_id, UNIX_TIMESTAMP(c.date) as date, c.interests, s.name as standard, ".
			         "(select count(*) from case_scope where case_id=c.id) as scope_count, ".
			         "(select count(*) from case_threats where case_id=c.id) as threat_count, ".
					 "(select count(*) from case_threat_control m, case_threats t where m.case_threat_id=t.id and t.case_id=c.id) as control_count, ".
			         "(select count(*) from case_scenarios where case_id=c.id) as scenario_count, ".
			         "(select count(*) from case_progress where case_id=c.id) as progress_count ".
			         "from cases c, standards s ".
			         "where c.standard_id=s.id and c.organisation_id=%d and archived=%d order by date desc";

			if (($cases = $this->db->execute($query, $this->organisation_id, $archive ? YES : NO)) === false) {
				return false;
			}

			foreach ($cases as $c => $case) {
				$query = "select *, (select count(*) from case_threat_control m where m.case_threat_id=t.id) as control_count ".
				         "from case_threats t where t.case_id=%d";
				if (($threats = $this->db->execute($query, $case["id"])) === false) {
					return false;
				}

				$cases[$c]["controls_ok"] = true;
				foreach ($threats as $threat) {
					if ((($threat["control_count"] != 0) && ($threat["handle"] == THREAT_ACCEPT)) ||
						(($threat["control_count"] == 0) && ($threat["handle"] != THREAT_ACCEPT))) {
						$cases[$c]["controls_ok"] = false;
						break;
					}
				}
			}

			foreach (array_keys($cases) as $key) {
				$this->decrypt($cases[$key], "name", "organisation", "scope", "impact");
			}

			return $cases;
		}

		public function count_archive() {
			$query = "select count(*) as count from cases where organisation_id=%d and archived=%d";

			if (($result = $this->db->execute($query, $this->user->organisation_id, YES)) === false) {
				return false;
			}

			return $result[0]["count"];
		}

		public function get_standards() {
			$query = "select * from standards";
			if ($this->user->is_admin == false) {
				$query .= " where active=%d";
			}
			$query .= " order by id desc";

			return $this->db->execute($query, YES);
		}

		public function start_crumb($case) {
			if ($case["scope_count"] == 0) {
				return "case/scope";
			}

			if ($case["interests"] == "") {
				return "case/interests";
			}

			if ($case["threat_count"] < 5) {
				return "case/threats";
			}

			if ($case["controls_ok"] == false) {
				return "case/controls";
			}

			if ($case["scenario_count"] == 0) {
				return "case/scenarios";
			}

			if ($case["progress_count"] > 0) {
				return "case/progress";
			}

			return "case/report";
		}

		public function save_oke($case) {
			$result = true;

			if (trim($case["name"]) == "") {
				$this->view->add_message("Specify the case name.");
				$result = false;
			}

			if (trim($case["organisation"]) == "") {
				$this->view->add_message("Specify the organization name.");
				$result = false;
			}

			if ($result) {
				$query = "select count(*) as count from cases where name=%s and organisation=%s and organisation_id=%d";
				$params = array($case["name"], $case["organisation"], $this->organisation_id);

				if (isset($case["id"])) {
					$query .= " and id!=%d";
					array_push($params, $case["id"]);
				}

				if (($result = $this->db->execute($query, $params)) === false) {
					return false;
				}
				if ($result[0]["count"] > 0) {
					$this->view->add_message("A case with that name already exists for the specified organization(al unit).");
					$result = false;
				}
			}

			return $result;
		}

		public function create_case($case) {
			$keys = array("id", "organisation_id", "standard_id", "name", "organisation", "date", "scope", "impact", "interests", "logo", "archived");

			$case["id"] = null;
			$case["organisation_id"] = $this->organisation_id;
			$case["interests"] = "";
			if (trim($case["logo"]) == "") {
				$case["logo"] = null;
			}
			$case["archived"] = NO;

			$this->encrypt($case, "name", "organisation", "scope", "impact");

			return $this->db->insert("cases", $case, $keys);
		}

		public function update_case($case) {
			$keys = array("standard_id", "name", "organisation", "date", "scope", "impact", "logo", "archived");

			if (trim($case["logo"]) == "") {
				$case["logo"] = null;
			}
			$case["archived"] = is_true($case["archived"] ?? false) ? YES : NO;

			$this->encrypt($case, "name", "organisation", "scope", "impact");

			return $this->db->update("cases", $case["id"], $case, $keys);
		}

		public function delete_case($case_id) {
			$queries = array(
				array("delete from case_progress where case_id=%d", $case_id),
				array("delete from case_scenarios where case_id=%d", $case_id),
				array("delete from case_threat_control where case_threat_id in (select id from case_threats where case_id=%d)", $case_id),
				array("delete from case_threat_bia where case_threat_id in (select id from case_threats where case_id=%d)", $case_id),
				array("delete from case_threats where case_id=%d", $case_id),
				array("delete from case_scope where case_id=%d", $case_id),
				array("delete from cases where id=%d", $case_id));

			return $this->db->transaction($queries);
		}
	}
?>
