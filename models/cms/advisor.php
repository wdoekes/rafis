<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class cms_advisor_model extends Banshee\model {
		public function get_advisors() {
			$query = "select a.id, u.fullname, o.name as organisation, a.crypto_key ".
			         "from advisors a, users u ".
			         "left join organisations o on o.id=u.organisation_id ".
			         "where a.user_id=u.id and a.organisation_id=%d";

			return $this->db->execute($query, $this->user->organisation_id);
		}

		public function activate_advisor($id) {
			$query = "select * from advisors where id=%d and organisation_id=%d and crypto_key is null";
			if (($result = $this->db->execute($query, $id, $this->user->organisation_id)) === false) {
				return false;
			}
			$advisor = $result[0];

			$rsa = new \Banshee\Protocol\RSA($advisor["public_key"]);
			$cookie = new \Banshee\secure_cookie($this->settings);
			$data = array("crypto_key" => $rsa->encrypt_with_public_key($cookie->crypto_key));

			$this->user->log_action("accepted advisor request for ".$advisor["user_id"]);

			return $this->db->update("advisors", $id, $data) !== false;
		}

		public function delete_advisor($id) {
			$query = "delete from advisors where id=%d and organisation_id=%d";

			return $this->db->query($query, $id, $this->user->organisation_id) !== false;
		}
	}
?>
