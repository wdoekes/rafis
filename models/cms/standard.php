<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class cms_standard_model extends Banshee\tablemanager_model {
		protected $table = "standards";
		protected $elements = array(
			"name" => array(
				"label"    => "Standaard",
				"type"     => "varchar",
				"overview" => true,
				"required" => true),
			"active" => array(
				"label"    => "Active",
				"type"     => "boolean",
				"overview" => true,
				"default"  => true));
		
		public function delete_oke($item_id) {
			$query = "select count(*) as count from cases where standard_id=%d";

			if (($result = $this->db->execute($query, $item_id)) === false) {
				return false;
			}

			if ($result[0]["count"] > 0) {
				$this->view->add_message("This standard is being used in a case.");
				return false;
			}

			return true;
		}

		public function delete_item($item_id) {
			if ($_SESSION["standard"] == $item_id) {
				$_SESSION["standard"] = 1;
			}

			$this->db->query("begin");

			$query = "delete from mitigation where control_id in (select id from controls where standard_id=%d)";
			if ($this->db->query($query, $item_id) === false) {
				$this->db->query("rollback");
				return false;
			}

			$query = "delete from controls where standard_id=%d";
			if ($this->db->query($query, $item_id) === false) {
				$this->db->query("rollback");
				return false;
			}

			$query = "delete from control_categories where standard_id=%d";
			if ($this->db->query($query, $item_id) === false) {
				$this->db->query("rollback");
				return false;
			}

			if (parent::delete_item($item_id) === false) {
				$this->db->query("rollback");
				return false;
			}

			return $this->db->query("commit") !== false;
		}
	}
?>
