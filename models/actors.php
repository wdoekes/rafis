<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class actors_model extends rafis_model {
		private $actor_threat = null;

		private function sort_actors($actor1, $actor2) {
			return strcmp($actor1["name"], $actor2["name"]);
		}

		public function get_actors() {
			$query = "select * from actors where organisation_id=%d order by name";

			if (($actors = $this->db->execute($query, $this->organisation_id)) === false) {
				return false;
			}

			foreach (array_keys($actors) as $key) {
				$this->decrypt($actors[$key], "name", "reason");
			}

			uasort($actors, array($this, "sort_actors"));

			return $actors;
		}

		public function actor_threat($actor) {
			if ($this->actor_threat === null) {
				$this->actor_threat = array();
				foreach (config_array(ACTOR_THREAT) as $i => $line) {
					$layer = explode(" - ", $line);
					$this->actor_threat[$i] = array();
					foreach ($layer as $j => $row) {
						$this->actor_threat[$i][$j] = explode(",", $row);
					}
				}
			}

			return $this->actor_threat[$actor["knowledge"] - 1][$actor["resources"] - 1][$actor["chance"] - 1];
		}

		public function get_actor($actor_id) {
			$query = "select * from actors where id=%d and organisation_id=%d limit 1";
			if (($result = $this->db->execute($query, $actor_id, $this->organisation_id)) == false) {
				return false;
			}

			$this->decrypt($result[0], "name", "reason");

			return $result[0];
		}

		public function save_oke($actor) {
			$result = true;

			if (trim($actor["name"]) == "") {
				$this->view->add_message("Specify the actor's name.");
				$result = false;
			}

			$pulldowns = array(
				"chance"	=> ACTOR_CHANCE,
				"knowledge" => ACTOR_KNOWLEDGE,
				"resources" => ACTOR_RESOURCES);

			foreach ($pulldowns as $variable => $options) {
				$max = count(config_array($options));
				if (($actor[$variable] < 1) || ($actor[$variable] > $max)) {
					$this->view->add_message("Ongeldige optie gekozen.");
					$result = false;
				}
			}

			return $result;
		}

		public function create_actor($actor) {
			$keys = array("id", "organisation_id", "name", "chance", "knowledge", "resources", "reason");

			$actor["id"] = null;
			$actor["organisation_id"] = $this->organisation_id;

			$this->encrypt($actor, "name", "reason");

			return $this->db->insert("actors", $actor, $keys);
		}

		public function update_actor($actor) {
			if ($this->get_actor($actor["id"]) == false) {
				return false;
			}

			$keys = array("name", "chance", "knowledge", "resources", "reason");

			$this->encrypt($actor, "name", "reason");

			return $this->db->update("actors", $actor["id"], $actor, $keys);
		}

		public function delete_actor($actor_id) {
			if ($this->get_actor($actor_id) == false) {
				$result = false;
			}

			$queries = array(
				array("update case_threats set actor_id=null where actor_id=%d", $actor_id),
				array("delete from actors where id=%d", $actor_id));

			return $this->db->transaction($queries) != false;
		}
	}
?>
