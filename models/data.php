<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class data_model extends rafis_model {
		private function export_table($table, $column, $value) {
			$query = "select * from %S where %S=%s";

			return $this->db->execute($query, $table, $column, $value);
		}

		private function userid_to_username($user_id) {
			if ($user_id == null) {
				return null;
			}

			if (($user = $this->db->entry("users", $user_id)) == false) {
				return false;
			}

			if ($user["organisation_id"] != $this->organisation_id) {
				return false;
			}

			return $user["username"];
		}

		private function username_to_userid($username) {
			if ($username == null) {
				return null;
			}

			if (($user = $this->db->entry("users", $username, "username")) == false) {
				return false;
			}

			if ($user["organisation_id"] != $this->organisation_id) {
				return null;
			}

			return (int)$user["id"];
		}

		public function get_export() {
			$export = array();

			if (($export["bia"] = $this->export_table("bia", "organisation_id", $this->organisation_id)) === false) {
				return false;
			}

			if (($export["actors"] = $this->export_table("actors", "organisation_id", $this->organisation_id)) === false) {
				return false;
			}

			if (($export["cases"] = $this->export_table("cases", "organisation_id", $this->organisation_id)) === false) {
				return false;
			}

			$export["case_scope"] = array();
			$export["case_threats"] = array();
			$export["case_threat_bia"] = array();
			$export["case_threat_control"] = array();
			$export["case_scenarios"] = array();
			$export["case_progress"] = array();

			foreach ($export["cases"] as $case) {
				if (($scope = $this->export_table("case_scope", "case_id", $case["id"])) === false) {
					return false;
				}
				$export["case_scope"] = array_merge($export["case_scope"], $scope);

				if (($threats = $this->export_table("case_threats", "case_id", $case["id"])) === false) {
					return false;
				}
				$export["case_threats"] = array_merge($export["case_threats"], $threats);

				foreach ($threats as $threat) {
					if (($threat_bia = $this->export_table("case_threat_bia", "case_threat_id", $threat["id"])) === false) {
						return false;
					}
					$export["case_threat_bia"] = array_merge($export["case_threat_bia"], $threat_bia);

					if (($threat_control = $this->export_table("case_threat_control", "case_threat_id", $threat["id"])) === false) {
						return false;
					}
					$export["case_threat_control"] = array_merge($export["case_threat_control"], $threat_control);
				}

				if (($scenarios = $this->export_table("case_scenarios", "case_id", $case["id"])) === false) {
					return false;
				}
				$export["case_scenarios"] = array_merge($export["case_scenarios"], $scenarios);

				if (($progress = $this->export_table("case_progress", "case_id", $case["id"])) === false) {
					return false;
				}
				foreach ($progress as $key => $item) {
					foreach (array("executor_id", "reviewer_id") as $column) {
						if (($progress[$key][$column] = $this->userid_to_username($item[$column])) === false) {
							return false;
						}
					}
				}
				$export["case_progress"] = array_merge($export["case_progress"], $progress);
			}

			$decrypt = array(
				"bia"            => array("item", "description", "impact"),
				"actors"         => array("name", "reason"),
				"cases"          => array("name", "organisation", "scope", "impact", "interests"),
				"case_threats"   => array("threat", "action", "current", "argumentation"),
				"case_scenarios" => array("title", "scenario", "consequences"),
				"case_progress"  => array("info"));

			foreach ($decrypt as $table => $columns) {
				foreach (array_keys($export[$table]) as $key) {
					$this->decrypt($export[$table][$key], $columns);
				}
			}

			return $export;
		}

		public function signature($export) {
			$hash = hash("sha256", json_encode($export));

			return hash_hmac("sha256", $hash, $this->settings->secret_website_code);
		}

		public function generate_filename($str) {
			$valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789() ";

			$result = "";
			$len = strlen($str);
			for ($i = 0; $i < $len; $i++) {
				$c = substr($str, $i, 1);
				if (strpos($valid, $c) !== false) {
					$result .= $c;
				}
			}

			return $result;
		}

		private function set_organisation($data, $table) {
			foreach ($data[$table] as $i => $item) {
				$data[$table][$i]["organisation_id"] = $this->organisation_id;
			}

			return $data;
		}

		private function import_table($table, $data, $foreign_keys, &$inserted_keys) {
			foreach ($data as $item) {
				$id = null;
				if (isset($item["id"])) {
					$id = (int)$item["id"];
					$item["id"] = null;
				}

				foreach ($foreign_keys as $column => $other_table) {
					if ($item[$column] === null) {
						continue;
					}

					if (($item[$column] = $inserted_keys[$other_table][$item[$column]]) == null) {
						return false;
					}
				}

				if ($this->db->insert($table, $item) === false) {
					return false;
				}

				if ($id !== null) {
					$inserted_keys[$table][$id] = $this->db->last_insert_id;
				}
			}

			return true;
		}

		public function import_data($data) {
			/* Fix accounts for case_progress
			 */	
			$missing = array();
			foreach ($data["case_progress"] as $key => $item) {
				$columns = array(
					"executor_id" => "uitvoerder",
					"reviewer_id" => "controleur");
				foreach ($columns as $column => $role) {
					$username = $item[$column];
					if (($data["case_progress"][$key][$column] = $this->username_to_userid($item[$column])) === false) {
						$data["case_progress"][$key][$column] = null;
						$info = &$data["case_progress"][$key]["info"];
						if (trim($info) != "") {
							$info = trim($info)."\n";
						}
						$info .= "\nIMPORT: de ".$role." van deze taak was '".$username."'.";
						array_push($missing, $username);
					}
				}
			}

			if (count($missing) > 0) {
				$this->view->add_system_warning("The export progress data contained references to users who are not present in the system. These are users with the following usernames: ".implode(", ", $missing).". The missing roles have been added in the information field of the respective progress tasks.");
				$this->view->add_system_warning("You can choose to create the missing users and re-import the export. The progress tasks will then be assigned to those users.");
			}

			/* Import data
			 */
			$query = "select * from cases where organisation_id=%d";
			if (($cases = $this->db->execute($query, $this->organisation_id)) === false) {
				return false;
			}

			$this->db->query("begin");

			/* Delete data
			 */
			foreach ($cases as $case) {
				if ($this->borrow("case")->delete_case($case["id"]) === false) {
					$this->db->query("rollback");
					return false;
				}
			}

			$flush = array("actors", "bia");
			foreach ($flush as $table) {
				$query = "delete from %S where organisation_id=%d";
				if ($this->db->query($query, $table, $this->organisation_id) === false) {
					$this->db->query("rollback");
					return false;
				}
			}

			/* Adjust data
			 */
			$data = $this->set_organisation($data, "bia");
			$data = $this->set_organisation($data, "actors");
			$data = $this->set_organisation($data, "cases");

			$encrypt = array(
				"bia"            => array("item", "description", "impact"),
				"actors"         => array("name", "reason"),
				"cases"          => array("name", "organisation", "scope", "impact", "interests"),
				"case_threats"   => array("threat", "action", "current", "argumentation"),
				"case_scenarios" => array("title", "scenario", "consequences"),
				"case_progress"  => array("info"));

			foreach ($encrypt as $table => $columns) {
				if (is_array($data[$table]) == false) {
					continue;
				}

				foreach (array_keys($data[$table]) as $key) {
					$this->encrypt($data[$table][$key], $columns);
				}
			}

			/* Store data
			 */
			$tables = array(
				"bia"                 => array(),
				"actors"              => array(),
				"cases"               => array(),
				"case_scope"          => array("case_id" => "cases", "bia_id" => "bia"),
				"case_threats"        => array("case_id" => "cases", "actor_id" => "actors"),
				"case_threat_bia"     => array("case_threat_id" => "case_threats", "bia_id" => "bia"),
				"case_threat_control" => array("case_threat_id" => "case_threats"),
				"case_scenarios"      => array("case_id" => "cases"),
				"case_progress"       => array("case_id" => "cases"));

			$inserted_keys = array();
			foreach (array_keys($tables) as $table) {
				$inserted_keys[$table] = array();
			}

			foreach ($tables as $table => $foreign_keys) {
				if (is_array($data[$table]) == false) {
					continue;
				}

				if ($this->import_table($table, $data[$table], $foreign_keys, $inserted_keys) == false) {
					$this->db->query("rollback");
					return false;
				}
			}

			return $this->db->query("commit") !== false;
		}
	}
?>
