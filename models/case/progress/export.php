<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class case_progress_export_model extends rafis_model {
		private function sort_progress($item_a, $item_b) {
			return version_compare($item_a["number"], $item_b["number"]);
		}

		private function get_progress($case_id) {
			if (($progress = $this->borrow("case/progress")->get_case_controls($case_id)) === false) {
				return false;
			}

			uasort($progress, array($this, "sort_progress"));

			return $progress;
		}

		public function get_export_csv($case) {
			if (($progress = $this->get_progress($case["id"])) === false) {
				return false;
			}

			if (($standard = $this->get_standard($case["standard_id"])) === false) {
				return false;
			}

			$csv = new \Banshee\csvfile();
			$csv->add_line("#", "Control from ".$standard, "Urgency", "Assigned to", "Deadline", "Done", "Hours planned", "Hours invested", "Information");
			foreach ($progress as $task) {
				$task["name"] = utf8_encode($task["name"]);
				if ($task["deadline"] != "") {
					$task["deadline"] = date_string("j F Y", $task["deadline"]);
				}
				$task["done"] = is_true($task["done"]) ? "yes" : "no";

				$line = array($task["number"], $task["name"], $task["urgency"], $task["person"], $task["deadline"], $task["done"], $task["hours_planned"] + 0, $task["hours_invested"] + 0, $task["info"]);
				$csv->add_line($line);
			}

			return $csv;
		}
	}
?>
