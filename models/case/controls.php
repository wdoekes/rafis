<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class case_controls_model extends rafis_model {
		private function sort_threats($threat1, $threat2) {
			return strcmp($threat1["threat"], $threat2["threat"]);
		}

		public function get_case_threats($case_id) {
			$query = "select t.*, (select count(*) from case_threat_control s, cases c, controls m ".
			         "where s.case_threat_id=t.id and t.case_id=c.id and s.control_id=m.id ".
			         "and c.standard_id=m.standard_id) as controls from case_threats t where case_id=%d";

			if (($threats = $this->db->execute($query, $case_id)) === false) {
				return false;
			}

			foreach (array_keys($threats) as $key) {
				$this->decrypt($threats[$key], "threat", "action", "current", "argumentation");
			}

			uasort($threats, array($this, "sort_threats"));

			return $threats;
		}

		public function get_case_threat($threat_id, $case_id) {
			return $this->borrow("case/threats")->get_case_threat($threat_id, $case_id);
		}

		public function get_selected_controls($threat_id, $case_id) {
			$query = "select s.* from case_threat_control s, case_threats t, cases c, controls m ".
			         "where s.case_threat_id=t.id and t.id=%d and t.case_id=c.id and c.id=%d ".
			         "and s.control_id=m.id and m.standard_id=c.standard_id";

			if (($selected = $this->db->execute($query, $threat_id, $case_id)) === false) {
				return false;
			}

			$result = array();
			foreach ($selected as $item) {
				array_push($result, $item["control_id"]);
			}

			return $result;
		}

		public function get_controls_standard($standard_id) {
			$query = "select * from control_categories where standard_id=%d order by number";
			if (($result = $this->db->execute($query, $standard_id)) === false) {
				return false;
			}

			$categories = array();
			foreach ($result as $item) {
				$categories[$item["number"]] = $item;
			}

			$query = "select * from controls where standard_id=%d order by id";
			if (($controls = $this->db->execute($query, $standard_id, ".")) === false) {
				return false;
			}

			$category = 0;
			foreach ($controls as $m => $control) {
				list($number) = explode(".", $control["number"], 2);
				if ($number != $category) {
					$category = $number;
					$controls[$m]["category"] = $categories[$category];
				}
			}

			return $controls;
		}

		public function get_controls_threat($threat_id) {
			$query = "select * from controls m, mitigation t ".
			         "where m.id=t.control_id and t.threat_id=%d";

			return $this->db->execute($query, $threat_id);
		}

		public function get_threats() {
			$query = "select * from threats order by number";

			return $this->db->execute($query);
		}

		public function effective_control($control_reduce, $threat_handle) {
			/* x = control_reduce: chance|impact|chance & impact
			 * y = threat_handle:  control|avoid|resist|accept
			 */
			$effective = array(
				array(YES, YES, YES),
				array(YES,  NO, YES),
				array( NO, YES, YES),
				array( NO,  NO,  NO));

			return $effective[$threat_handle - 1][$control_reduce];
		}

		public function save_controls($controls, $case) {
			if ($this->get_case_threat($controls["case_threat_id"], $case["id"]) == false) {
				return false;
			}

			$this->db->query("begin");

			$query = "delete from case_threat_control where case_threat_id=%d and control_id in ".
			         "(select id from controls where standard_id=%d)";
			if ($this->db->query($query, $controls["case_threat_id"], $case["standard_id"]) === false) {
				$this->db->query("rollback");
				return false;
			}

			if (is_array($controls["selected"] ?? null)) {
				$data = array("case_threat_id" => $controls["case_threat_id"]);
				foreach ($controls["selected"] as $control_id) {
					$data["control_id"] = $control_id;
					if ($this->db->insert("case_threat_control", $data) === false) {
						$this->db->query("rollback");
						return false;
					}
				}
			}

			return $this->db->query("commit") !== false;
		}
	}
?>
