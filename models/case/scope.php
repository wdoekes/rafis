<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class case_scope_model extends rafis_model {
		public function get_scope($case_id) {
			if (($items = $this->borrow("bia")->get_items($case_id)) === false) {
				return false;
			}

			$query = "select bia_id from case_scope where case_id=%d";
			if (($result = $this->db->execute($query, $case_id)) === false) {
				return false;
			}

			$scope = array();
			foreach ($result as $record) {
				array_push($scope, $record["bia_id"]);
			}

			foreach ($items as $i => $item) {
				$items[$i]["scope"] = in_array($item["id"], $scope) ? YES : NO;
			}

			return $items;
		}

		public function save_scope($scope, $case_id) {
			$this->db->query("begin");

			$query = "delete from case_scope where case_id=%d";
			if ($this->db->query($query, $case_id) === false) {
				return false;
			}

			if (is_array($scope) == false) {
				return $this->db->query("commit") !== false;
			}

			$data = array("case_id" => $case_id);
			foreach ($scope as $item_id) {
				if ($this->borrow("bia")->get_item($item_id) == false) {
					$this->db->query("rollback");
					return false;
				}

				$data["bia_id"] = $item_id;
				if ($this->db->insert("case_scope", $data) === false) {
					$this->db->query("rollback");
					return false;
				}
			}

			return $this->db->query("commit") !== false;
		}
	}
?>
