<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class case_threats_model extends rafis_model {
		private function sort_threats($threat1, $threat2) {
			return strcmp($threat1["threat"], $threat2["threat"]);
		}

		public function get_case_threats($case_id) {
			$query = "select * from case_threats where case_id=%d order by id";

			if (($threats = $this->db->execute($query, $case_id)) === false) {
				return false;
			}

			foreach (array_keys($threats) as $key) {
				$this->decrypt($threats[$key], "threat", "action", "current", "argumentation");
			}

			uasort($threats, array($this, "sort_threats"));

			return $threats;
		}

		public function get_categories() {
			return $this->borrow("cms/threat")->get_categories();
		}

		public function get_case_threat($threat_id, $case_id) {
			$query = "select * from case_threats where id=%d and case_id=%d";
			if (($result = $this->db->execute($query, $threat_id, $case_id)) == false) {
				return false;
			}
			$threat = $result[0];

			$query = "select bia_id from case_threat_bia where case_threat_id=%d";
			if (($result = $this->db->execute($query, $threat_id)) === false) {
				return false;
			}
			$threat["threat_scope"] = array();
			foreach ($result as $item) {
				array_push($threat["threat_scope"], $item["bia_id"]);
			}

			$this->decrypt($threat, "threat", "action", "current", "argumentation");

			return $threat;
		}

		public function get_threat_templates() {
			$query = "select * from threats order by number";

			return $this->db->execute($query);
		}

		public function get_threat_template($threat_id, $standard_id) {
			if (($template = $this->db->entry("threats", $threat_id)) == false) {
				return false;
			}

			$query = "select name from standards where id=%d";
			if (($standard = $this->db->execute($query, $standard_id)) === false) {
				return false;
			}
			$template["standard"] = $standard[0]["name"];

			$query = "select m.* from controls m, mitigation g where m.id=g.control_id and g.threat_id=%d and m.standard_id=%d";
			if (($template["controls"] = $this->db->execute($query, $template["id"], $standard_id)) === false) {
				return false;
			}

			return $template;
		}

		private function sort_scope($bia1, $bia2) {
			return strcmp($bia1["item"], $bia2["item"]);
		}

		public function get_scope($case_id) {
			$query = "select b.* from bia b, case_scope s where b.id=s.bia_id and s.case_id=%d";

			if (($scope = $this->db->execute($query, $case_id)) === false) {
				return false;
			}

			foreach (array_keys($scope) as $key) {
				$this->decrypt($scope[$key], "item", "description", "impact");
			}

			uasort($scope, array($this, "sort_scope"));

			return $scope;
		}

		public function get_actors() {
			if (($actors = $this->borrow("actors")->get_actors()) === false) {
				return false;
			}

			$actor_threat = config_array(ACTOR_THREAT_LABELS);

			foreach ($actors as $i => $actor) {
				$threat_level = $this->borrow("actors")->actor_threat($actor);
				$actors[$i]["threat"] = $actor_threat[$threat_level];
			}

			return $actors;
		}

		public function save_oke($threat) {
			$result = true;

			if (trim($threat["threat"]) == "") {
				$this->view->add_message("Specify the threat.");
				$result = false;
			}

			if (($threat["chance"] < 1) || ($threat["chance"] > 5)) {
				$this->view->add_message("Select the estimated chance.");
				$result = false;
			}

			if (($threat["impact"] < 1) || ($threat["impact"] > 5)) {
				$this->view->add_message("Select the estimated impact.");
				$result = false;
			}

			if (($threat["handle"] < 1) || ($threat["handle"] > 4)) {
				$this->view->add_message("Select the desired approach.");
				$result = false;
			}

			return $result;
		}

		private function save_threat_scope($scope, $threat_id, $case_id) {
			if ($this->get_case_threat($threat_id, $case_id) == false) {
				return false;
			}

			$query = "delete from case_threat_bia where case_threat_id=%d";
			if ($this->db->query($query, $threat_id) === false) {
				return false;
			}

			if (is_array($scope) == false) {
				return true;
			}

			$query = "select bia_id from case_scope where case_id=%d";
			if (($result = $this->db->execute($query, $case_id)) === false) {
				return false;
			}
			$bia_scope = array();
			foreach ($result as $item) {
				array_push($bia_scope, $item["bia_id"]);
			}

			$data = array("case_threat_id" => $threat_id);
			foreach ($scope as $bia_id) {
				if (in_array($bia_id, $bia_scope)) {
					$data["bia_id"] = $bia_id;
					if ($this->db->insert("case_threat_bia", $data) === false) {
						return false;
					}
				}
			}

			return true;
		}

		public function create_threat($threat, $case) {
			$keys = array("id", "case_id", "threat", "actor_id", "chance", "impact", "handle", "action", "current", "argumentation");

			$threat["id"] = null;
			$threat["case_id"] = $case["id"];
			if ($threat["actor_id"] == 0) {
				$threat["actor_id"] = null;
			}

			$this->encrypt($threat, "threat", "action", "current", "argumentation");

			$this->db->query("begin");

			if ($this->db->insert("case_threats", $threat, $keys) === false) {
				$this->db->query("rollback");
				return false;
			}
			$threat_id = $this->db->last_insert_id;

			if ($this->save_threat_scope($threat["threat_scope"] ?? null, $threat_id, $case["id"]) == false) {
				$this->db->query("rollback");
				return false;
			}

			if (isset($threat["template_id"])) {
				$data = array("case_threat_id" => $this->db->last_insert_id);

				if (($template = $this->get_threat_template($threat["template_id"], $case["standard_id"])) == false) {
					$this->db->query("rollback");
					return false;
				}

				foreach ($template["controls"] as $control) {
					$data["control_id"] = $control["id"];
					if ($this->db->insert("case_threat_control", $data) === false) {
						$this->db->query("rollback");
						return false;
					}
				}
			}

			return $this->db->query("commit") !== false;
		}

		public function update_threat($threat, $case_id) {
			if ($this->get_case_threat($threat["id"], $case_id) == false) {
				return false;
			}

			if ($threat["actor_id"] == 0) {
				$threat["actor_id"] = null;
			}

			$this->encrypt($threat, "threat", "action", "current", "argumentation");

			$keys = array("threat", "actor_id", "chance", "impact", "handle", "action", "current", "argumentation");
			if ($this->db->update("case_threats", $threat["id"], $threat, $keys) === false) {
				$this->db->query("rollback");
				return false;
			}

			if ($this->save_threat_scope($threat["threat_scope"] ?? null, $threat["id"], $case_id) === false) {
				$this->db->query("rollback");
				return false;
			}

			return $this->db->query("commit") !== false;
		}

		public function delete_oke($threat) {
			$result = true;

			return $result;
		}

		public function delete_threat($threat_id, $case_id) {
			if ($this->get_case_threat($threat_id, $case_id) == false) {
				return false;
			}

			$queries = array(
				array("delete from case_threat_control where case_threat_id=%d", $threat_id),
				array("delete from case_threat_bia where case_threat_id=%d", $threat_id),
				array("delete from case_threats where id=%d", $threat_id));

			return $this->db->transaction($queries) !== false;
		}
	}
?>
