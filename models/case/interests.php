<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class case_interests_model extends rafis_model {
		public function save_interests($interests, $case_id) {
			$data = array("interests" => $interests);

			$this->encrypt($data, "interests");

			return $this->db->update("cases", $case_id, $data) !== false;
		}

		public function add_bia(&$pdf, $bia_items) {
			foreach ($bia_items as $nr => $item) {
				if ($item["scope"] == NO) {
					continue;
				}

				$pdf->SetFillColor(232, 232, 232);
				$pdf->SetFont("helvetica", "B", 10);
				$pdf->Cell(0, 5, $item["item"], 0, 0, "", true);
				$pdf->SetFont("helvetica", "", 10);
				$pdf->Ln(5);

				if ($item["description"] != "") {
					$pdf->SetLeftMargin(19);
					$pdf->Write(5, $item["description"]);
					$pdf->SetLeftMargin(15);
					$pdf->Ln(5);
				}
				$pdf->SetFont("helvetica", "", 9);
				$pdf->Cell(4, 5);
				$pdf->Cell(28, 5, "C: ".($this->confidentiality_score[$item["confidentiality"] - 1] ?? ""));
				$pdf->Cell(23, 5, "I: ".($this->integrity_score[$item["integrity"] - 1] ?? ""));
				$pdf->Cell(24, 5, "A: ".($this->availability_score[$item["availability"] - 1] ?? ""));
				$pdf->Cell(35, 5, "Value: ".$this->asset_value_labels[$item["value"]]);
				$pdf->Cell(17, 5, "PII: ".(is_true($item["personal_data"]) ? "yes" : "no"));
				$pdf->Cell(25, 5, "Owner: ".(is_true($item["owner"]) ? "yes" : "no"));
				$pdf->Cell(0, 5, "Location: ".$item["location"]);
				$pdf->Ln(5);
				$pdf->SetFont("helvetica", "", 10);

				$pdf->AddTextBlock("Impact of an incident", $item["impact"]);
				$pdf->Ln(1);
			}

			return true;
		}

		public function add_actors(&$pdf, $case_id) {
			if (($actors = $this->borrow("actors")->get_actors()) === false) {
				return false;
			}

			$pdf->SetFont("helvetica", "B", 11);
			$pdf->Cell(55, 6, "Name", "B");
			$pdf->Cell(44, 6, "Willingness / chance", "B");
			$pdf->Cell(26, 6, "Knowledge", "B");
			$pdf->Cell(25, 6, "Resources", "B");
			$pdf->Cell(30, 6, "Threats", "B");
			$pdf->Ln(8);

			$actor_chance = config_array(ACTOR_CHANCE);
			$actor_knowledge = config_array(ACTOR_KNOWLEDGE);
			$actor_resources = config_array(ACTOR_RESOURCES);
			$actor_threat = config_array(ACTOR_THREAT_LABELS);

			$pdf->SetFont("helvetica", "", 10);
			foreach ($actors as $actor) {
				$pdf->Cell(55, 5, $actor["name"]);
				$pdf->Cell(44, 5, $actor_chance[$actor["chance"] - 1]);
				$pdf->Cell(26, 5, $actor_knowledge[$actor["knowledge"] - 1]);
				$pdf->Cell(25, 5, $actor_resources[$actor["resources"] - 1]);
				$threat_level = $this->borrow("actors")->actor_threat($actor);
				$pdf->Cell(30, 5, $actor_threat[$threat_level]);
				$pdf->Ln(5);
				if (trim($actor["reason"]) != "") {
					$pdf->Cell(5, 5, "");
					$pdf->Cell(14, 5, "Reason: ");
					$pdf->Cell(161, 5, $actor["reason"]);
					$pdf->Ln(5);
				}
			}
			$pdf->Ln(10);

			return true;
		}

		private function capitalize_first_letter($word) {
			$word[0] = strtoupper($word[0]);

			return $word;
		}

		public function make_handout($case) {
			if (($bia_items = $this->borrow("case/scope")->get_scope($case["id"])) === false) {
				return false;
			}

			if (($templates = $this->borrow("case/threats")->get_threat_templates()) == false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($categories = $this->borrow("case/threats")->get_categories()) == false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$pdf = new RAFIS_report($case["title"], true);
			$pdf->SetAuthor($this->get_organisation($this->user->organisation_id)." en RAFIS");
			$pdf->SetSubject("Overview of interests, scope and actors");
			$pdf->SetKeywords("RAFIS, handout, Business Impact Analyse, actors");
			$pdf->AliasNbPages();

			/* Information systems
			 */
			$pdf->AddPage();
			$pdf->SetFont("helvetica", "B", 15);
			$pdf->Cell(0, 5, "Handout for ".$case["name"], 0, 1, "C");
			if ($case["interests"] != "") {
				$pdf->Ln(8);
				$pdf->AddChapter("Interests");
				$pdf->Write(5, $case["interests"]);
			}
			$pdf->Ln(12);

			$pdf->AddChapter("Overview information systems");
			$pdf->Ln(3);

			$this->add_bia($pdf, $bia_items);

			/* Actors
			 */
			$pdf->AddPage();
			$pdf->AddChapter("Actors");

			$this->add_actors($pdf, $case["id"]);

			/* Chance
			 */
			$actor_threat = config_array(ACTOR_THREAT_LABELS);
			$frequency = config_array(CHANCE_FREQUENCY);
			$pdf->AddChapter("Determining the chance");
			$pdf->Write(5, "With limited measures against an actor, the chance may be reduced by one position. With sufficient measures against an actor, the chance may be decreased by two positions.");
			$pdf->Ln(7);
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(40, 5, "", "B");
			$pdf->Cell(45, 5, "Threat from actor", "B");
			$pdf->Cell(95, 5, "Frequency unintented incident", "B");
			$pdf->Ln(5);
			$pdf->SetFont("helvetica", "", 10);
			for ($i = count($this->risk_matrix_chance) - 1; $i>= 0; $i--) {
				$pdf->SetFont("helvetica", "B", 10);
				$pdf->Cell(40, 5, $this->capitalize_first_letter($this->risk_matrix_chance[$i]).": ");
				$pdf->SetFont("helvetica", "", 10);
				$pdf->Cell(45, 5, $actor_threat[$i]);
				$pdf->Cell(95, 5, $frequency[$i]);
				$pdf->Ln(5);
			}
			$pdf->Ln(10);

			/* Impact values
			 */
			$pdf->AddChapter("Interpretation of the impact");
			$impact_values = json_decode($case["impact"], true);
			for ($i = count($this->risk_matrix_impact) - 1; $i>= 0; $i--) {
				$pdf->SetFont("helvetica", "B", 10);
				$pdf->Cell(40, 5, $this->capitalize_first_letter($this->risk_matrix_impact[$i]).": ");
				$pdf->SetFont("helvetica", "", 10);
				$pdf->Write(5, $impact_values[$i]);
				$pdf->Ln(5);
			}
			$pdf->Ln(10);

			/* Action
			 */
			$pdf->AddChapter("Approaches to deal with a risk");
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(40, 5, "Control:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->Write(5, "Taking measures to lower the impact and chance of an incident.");
			$pdf->Ln(5);
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(40, 5, "Avoid:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->Write(5, "Taking measures to lower the chance of an incident.");
			$pdf->Ln(5);
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(40, 5, "Resist:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->Write(5, "Taking measures to lower the impact of an incident.");
			$pdf->Ln(5);
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(40, 5, "Accept:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->Write(5, "Accepting the impact and chance of an incident.");
			$pdf->Ln(15);

			/* Scales
			 */
			$pdf->AddChapter("BIA and actor scales");
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(40, 5, "Confidentiality:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(135, 5, implode(", ", config_array(CONFIDENTIALITY_SCORE)));
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(40, 5, "Integrity:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(135, 5, implode(", ", config_array(INTEGRITY_SCORE)));
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(40, 5, "Availablility:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(135, 5, implode(", ", config_array(AVAILABILITY_SCORE)));
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(40, 5, "Value:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(135, 5, implode(", ", config_array(ASSET_VALUE_LABELS)));

			$pdf->Ln(3);

			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(40, 5, "Willingness / chance:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(140, 5, implode(", ", config_array(ACTOR_CHANCE)));
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(40, 5, "Knowledge level:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(140, 5, implode(", ", config_array(ACTOR_KNOWLEDGE)));

			/* Threats
			 */
			$pdf->AddPage();
			$pdf->AddChapter("Possible threats");
			$pdf->Ln(2);

			$category_id = 0;
			foreach ($templates as $template) {
				if ($template["category_id"] != $category_id) {
					$category_id = $template["category_id"];
					$pdf->Ln(1);
					$pdf->SetFillColor(232, 232, 232);
					$pdf->SetFont("helvetica", "B", 10);
					$pdf->Cell(0, 5, $categories[$category_id]["name"], 0, 0, "", true);
					$pdf->SetFont("helvetica", "", 10);
					$pdf->Ln(5);
				}
				$pdf->Cell(7, 5, $template["number"].".");
				$pdf->Cell(0, 5, $template["threat"]);
				$pdf->Ln(5);
				$pdf->SetLeftMargin(22);
				$pdf->SetFont("helvetica", "I", 9);
				$pdf->Write(4, $template["description"]);
				$pdf->SetFont("helvetica", "", 10);
				$pdf->SetLeftMargin(15);
				$pdf->Ln(4);
			}

			return $pdf;
		}
	}
?>
