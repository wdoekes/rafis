<?php
	class case_risk_model extends rafis_model {
		public function session_exists($access_code) {
			$query = "select count(*) as count from  risk_assess_sessions where access_code=%d";

			if (($result = $this->db->execute($query, $access_code)) == false) {
				return false;
			}

			return $result[0]["count"] > 0;
		}

		public function create_session($access_code) {
			$data = array(
				"id"          => null,
				"access_code" => $access_code,
				"date"        => date("Y-m-d"));

			return $this->db->insert("risk_assess_sessions", $data) !== false;
		}

		public function delete_session($access_code) {
			$this->flush_votes($access_code);

			$query = "delete from risk_assess_sessions where access_code=%d";

			return $this->db->query($query, $access_code) !== false;
		}

		public function get_votes($access_code) {
			$query = "select chance, impact from risk_assess_values where risk_assess_session_id=".
			         "(select id from risk_assess_sessions where access_code=%d limit 1)";

			return $this->db->execute($query, $access_code);
		}

		public function flush_votes($access_code) {
			$query = "select id from risk_assess_sessions where access_code=%d limit 1";

			if (($sessions = $this->db->execute($query, $access_code)) == false) {
				return false;
			}
			$session = $sessions[0];

			$query = "delete from risk_assess_values where risk_assess_session_id=%d";

			return $this->db->query($query, $session["id"]) !== false;
		}
	}
?>
