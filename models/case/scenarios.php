<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class case_scenarios_model extends rafis_model {
		public function get_scenarios($case_id) {
			$query = "select * from case_scenarios where case_id=%d order by id";

			if (($scenarios = $this->db->execute($query, $case_id)) === false) {
				return false;
			}

			foreach (array_keys($scenarios) as $key) {
				$this->decrypt($scenarios[$key], "title", "scenario", "consequences");
			}

			return $scenarios;
		}

		public function get_actors() {
			return $this->borrow("actors")->get_actors();
		}

		public function get_bia() {
			return $this->borrow("bia")->get_items();
		}

		public function actor_threat($actor) {
			return $this->borrow("actors")->actor_threat($actor);
		}

		public function get_threats($case_id) {
			return $this->borrow("case/threats")->get_case_threats($case_id);
		}

		public function get_scenario($scenario_id, $case_id) {
			$query = "select * from case_scenarios where id=%d and case_id=%d";

			if (($result = $this->db->execute($query, $scenario_id, $case_id)) == false) {
				return false;
			}
			$scenario = $result[0];

			$this->decrypt($scenario, "title", "scenario", "consequences");

			return $scenario;
		}

		public function save_oke($scenario) {
			$result = true;

			if (trim($scenario["title"]) == "") {
				$this->view->add_message("Specify the title of the scenario.");
				$result = false;
			}

			if (trim($scenario["scenario"]) == "") {
				$this->view->add_message("Specify the scenario.");
				$result = false;
			}

			return $result;
		}

		public function create_scenario($scenario, $case_id) {
			$keys = array("id", "case_id", "title", "scenario", "consequences");

			$scenario["id"] = null;
			$scenario["case_id"] = $case_id;

			$this->encrypt($scenario, "title", "scenario", "consequences");

			return $this->db->insert("case_scenarios", $scenario, $keys);
		}

		public function update_scenario($scenario, $case_id) {
			if ($this->get_scenario($scenario["id"], $case_id) == false) {
				return false;
			}

			$keys = array("title", "scenario", "consequences");

			$this->encrypt($scenario, "title", "scenario", "consequences");

			return $this->db->update("case_scenarios", $scenario["id"], $scenario, $keys);
		}

		public function delete_scenario($scenario_id, $case_id) {
			if ($this->get_scenario($scenario_id, $case_id) == false) {
				return false;
			}

			return $this->db->delete("case_scenarios", $scenario_id);
		}
	}
?>
