<?php
	abstract class rafis_controller extends Banshee\controller {
		protected $case = null;
		protected $breadcrumbs = array(
			"case"             => "Cases",
			"case/scope/#"     => "Scope",
			"case/interests/#" => "Interests",
			"case/threats/#"   => "Threats",
			"case/controls/#"  => "Controls",
			"case/scenarios/#" => "Scenarios",
			"case/report/#"    => "Report",
			"case/progress/#"  => "Progress");

		public function __construct() {
			$arguments = func_get_args();
			call_user_func_array(array("parent", "__construct"), $arguments);

			$this->view->add_help_button();
		}

		protected function show_breadcrumbs($case_id) {
			$this->view->add_css("includes/breadcrumbs.css");

			$this->view->open_tag("breadcrumbs");
			foreach ($this->breadcrumbs as $link => $page) {
				$current = str_replace("/#", "", $link) == $this->page->page;
				$attr = array(
					"current" => show_boolean($current),
					"link"    => str_replace("#", $case_id, $link));
				$this->view->add_tag("crumb", $page, $attr);
			}
			$this->view->close_tag();
		}

		protected function valid_case_id($case_id) {
			if (valid_input($case_id, VALIDATE_NUMBERS, VALIDATE_NONEMPTY) == false) {
				$this->view->add_tag("result", "Geen casus opgegeven.", array("url" => $this->settings->start_page));
				return false;
			} else if (($this->case = $this->model->get_case($case_id)) == false) {
				$this->view->add_tag("result", "Onbekende casus.", array("url" => $this->settings->start_page));
				return false;
			}

			if ($this->case["organisation"] != "") {
				if (strtolower(substr($this->case["organisation"], 0, 3)) == "de ") {
					$organisation = substr($this->case["organisation"], 3);
				} else {
					$organisation = $this->case["organisation"];
				}

				$this->case["title"] = $organisation." :: ".$this->case["name"];
			} else {
				$this->case["title"] = $this->case["name"];
			}

			$this->view->add_tag("case", $this->case["title"], array("id" => $case_id));

			return true;
		}

		/* Generate a valid filename out of a string
		 *
		 * INPUT:  string test
		 * OUTPUT: string filename
		 * ERROR:  -
		 */
		protected function generate_filename($str) {
			$valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ";

			$len = strlen($str);
			for ($i = 0; $i < $len; $i++) {
				if (strpos($valid, substr($str, $i, 1)) === false) {
					$str = substr($str, 0, $i)."-".substr($str, $i + 1);
				}
			}

			return preg_replace('/-+/', "-", $str);
		}
	}
?>
