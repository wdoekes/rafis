<p>Hello [FULLNAME],</p>
<p>An account has been created for you at the <a href="[PROTOCOL]://[HOSTNAME]/">RAFIS website</a>. You can use the following credentials to login:</p>
<p>Username: [USERNAME]<br>Password: [PASSWORD]</p>
