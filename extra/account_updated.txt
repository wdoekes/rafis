<p>Hello [FULLNAME],</p>
<p>Your account at the <a href="[PROTOCOL]://[HOSTNAME]/">RAFIS website</a> has been updated. You can use the following credentials to login:</p>
<p>Username: [USERNAME]<br>Password: [PASSWORD]</p>
